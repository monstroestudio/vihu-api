<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h2>Olá {{$user->name}}</h2>
    <p>Segue abaixo o relatório de acesso solicitado de <strong>{{$data['request']['dataIni']}}</strong> até <strong>{{$data['request']['dataFim']}}</strong>:</p>
    <br>
    <table cellspacing=0 cellpadding=2 width="90%" style="border:1px solid #dfdfdf;border-collapse: collapse;">
        <thead>
            <tr>
                <th height="20" style="border:1px solid #ccc;border-collapse: collapse;">CONDOMÍNIO</th>
                <th height="20" style="border:1px solid #ccc;border-collapse: collapse;">NOME DO VISITANTE</th>
                <th height="20" style="border:1px solid #ccc;border-collapse: collapse;">CELULAR DO VISITANTE</th>
                <th height="20" style="border:1px solid #ccc;border-collapse: collapse;">DATA</th>   
                <th height="20" style="border:1px solid #ccc;border-collapse: collapse;">TIPO DE ACESSO</th>
                <th height="20" style="border:1px solid #ccc;border-collapse: collapse;">STATUS</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data['accesses'] as $item)
                <tr>
                    <td height="20" style="border:1px solid #ccc;border-collapse: collapse;">{{$item['condominium']['name']}}</td>
                    <td height="20" style="border:1px solid #ccc;border-collapse: collapse;">{{$item['visitor_name']}}</td>
                    <td height="20" style="border:1px solid #ccc;border-collapse: collapse;">{{$item['visitor_mobile']}}</td>
                    <td height="20" style="border:1px solid #ccc;border-collapse: collapse;">{{$item['date']}} {{$item['time']}}</td>
                    <td height="20" style="border:1px solid #ccc;border-collapse: collapse;">{{$item['type']}}</td>
                    <td height="20" style="border:1px solid #ccc;border-collapse: collapse;">{{$item['status']}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>