# Zenvia - Caminho do arquivo que contém a URL da API
- `/vendor/simplesoftwareio/simple-sms/src/Drivers/ZenviaSMS.php`

# CRONTABS

## Recicla códigos gerados e não usados a mais de 2 dias
- **Frequencia:** `Todo dia as 5:00`
- **URL a ser executada:** `https://api.vihu.com.br/api/internal/maintenance`

## Atualiza IP dos condominios
- **Frequencia:** `A cada 1 hora`
- **URL a ser executada:** `https://api.vihu.com.br/api/ddns/update`