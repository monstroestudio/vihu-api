<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DNS extends Model
{
    protected $table = 'dns';

    protected $fillable = [
        'condominiums_id',
        'dns',
        'ip'
    ];

    public function consominium() {
        return $this->hasOne('App\Condominium','id','condominiums_id');
    }    
}
