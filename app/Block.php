<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Block extends Model
{
    use SoftDeletes;

    protected $table = 'block';

    public function condominium() {
        return $this->hasOne('App\Condominium','id','condominiums_id');
    }
}
