<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SMS extends Model
{
    protected $table = 'sms';

    public function user(){
        return $this->hasOne('App\User', 'id', 'users_id');
    }

    public function access(){
        return $this->hasOne('App\Access');
    }
}
