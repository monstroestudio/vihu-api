<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Condominium extends Authenticatable
{
    use SoftDeletes;

    protected $table = 'condominiums';

    protected $hidden = [
        'password'
    ];    

    protected $casts = [
        'application_features' => 'array',
        'communication_type' => 'array'
    ];     

    public function resale() {
        return $this->hasOne('App\Resale','id','resale_id');
    }

    public function dns() {
        return $this->hasMany('App\DNS','condominiums_id');
    }

    public function blocks() {
        return $this->hasMany('App\Block','condominiums_id');
    }

    public function users() {
        return $this->belongsToMany('App\User', 'condominiums_users', 'condominiums_id', 'users_id');
    }    
}
