<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resale extends Model {
    use SoftDeletes;

    protected $table = "resale";

    public function condominiums() {
        return $this->hasMany('App\Condominium');
    }
}
