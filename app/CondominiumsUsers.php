<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CondominiumsUsers extends Model
{
    protected $table = 'condominiums_users';

    protected $fillable = [
        'condominiums_id',
        'users_id',
        'type',
    ];

    public function user(){
        return $this->hasOne('App\User', 'id', 'users_id');
    }    
}
