<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voice extends Model
{
    protected $table = 'voice';

    public function user(){
        return $this->hasOne('App\User');
    }

    public function access(){
        return $this->hasOne('App\Access');
    }
}
