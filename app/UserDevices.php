<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserDevices extends Authenticatable
{
    protected $table = 'user_devices';

    protected $fillable = [
        'data',
        'pin',
        'status'
    ];

    protected $hidden = [
        'pin'
    ];

    protected $casts = [
        'data' => 'array'
    ];
}
