<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Units extends Model
{
    use SoftDeletes;

    protected $casts = [
        'rfid_garage_keys' => 'array'
    ];

    public function condominium() {
        return $this->hasOne('App\Condominium','id','condominiums_id');
    }

    public function block() {
        return $this->hasOne('App\Block','id','block_id');
    }    

    public function vehicles()
    {
        return $this->hasMany('App\Vehicles', 'units_id');
    }    
}
