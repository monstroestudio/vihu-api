<?php

namespace App\Http\Controllers;

use Config;
use JWTAuth;
use App\Units;
use App\UnitsUser;
use App\DNS;
use App\User;
use App\UserDevices;
use App\Log;
use App\Condominium;
use App\CondominiumsUsers;
use App\Http\Controllers\SMSController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;

class ApiAuthController extends Controller
{
    public function authenticate(Request $request, SMSController $sms)
    {
        $uid = Carbon::now()->timestamp;

        Log::create(['uid'=>$uid, 'description' => 'Telefone:' .$request->login, 'type' => 'authenticate']);
        
        $user = User::where('mobile', $request->login)->with('condominiums')->with('units')->first();

        if (!is_object($user)) {
            return response()->json(['error' => 'Login ou senha inválido!'], 401);
        }

        try {
            $token = JWTAuth::attempt(['email'=>$user->email, 'password'=>$request->password]);
            if (!$token) {
                return response()->json(['error' => 'Login ou senha inválido!'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        //VERIFICA SE EXISTE ALGUM DEVICE ATIVADO
        $device = $user->devices()->where('status', 'active')->first();

        if (!$device || $device->data['uuid'] != $request->device['uuid']) {
            //VERIFICA SE ESTE DEVICE EXISTE NA TABELA
            $device = UserDevices::where('data->uuid', $request->device['uuid'])->first();

            //CASO NÃO EXISTIR, CRIA O DEVICE PARA O USUÁRIO E ENVIA O PIN DE VALIDAÇÃO
            if (!$device) {
                $userDevice = UserDevices::create([
                    'data' => $request->device,
                    'status' => 1,
                    'pin' => $this->generatePIN()
                ]);

                $user->device_id = $userDevice->id;
                $user->save();

                $sms->sendToValidation($user, $userDevice->pin, 'código VIHU para validação do seu Aplicativo.');
            } else {
                $device->status = 1;
                $device->pin = $this->generatePIN();
                $device->save();

                $user->device_id = $device->id;
                $user->save();

                $sms->sendToValidation($user, $device->pin, 'código VIHU para validação do seu Aplicativo.');
            }
        }

        $data = [
            "user_name" => $user->name,
            "user_cell" => $user->mobile,
            "user_email" => $user->email,
            "id_tipo_usuario" => 3,
            "panic" => $user->panic,
            "device" => $user->devices,           
            "condominios" => $user->condominiums->toArray(),
            "unidades" => $user->units->toArray(),
            "policy" => $user->policy
        ];

        return response()->json([
            "status" => "SUCCESS",
            "token"=> $token,
            "data" => $data
        ],200);
    }

    public function authenticate2(Request $request) {
        $credentials = $request->only('email', 'password');

        try {
            $token = JWTAuth::attempt($credentials);
            if (!$token) {
                return response()->json(['error'=>'invalid_credentials'], 401);
            }
        } catch(JWTException $e) {
            return response()->json(['error'=>'something_went_wrong'], 500);
        }

        $user = JWTAuth::toUser($token);

        return response()->json(['token'=>$token, 'user'=>$user], 200);
    }

    public function validateUser(User $user, $password)
    {
        if (decrypt($user->password) == $password) {
            return true;
        }

        return false;
    }

    public function validatePIN(Request $request) {
        $user = JWTAuth::toUser(JWTAuth::getToken());

        if (!$user) {
            return response()->json(['error' => 'Login ou senha inválido!'], 401);
        }

        $device = $user->devices()->where('status', 'pending')->first();
        if (!$device) {
            return response()->json(['error' => 'Não existem dispositivos pendentes para validação!'], 401);
        }

        if ($device->pin == $request->pin || $request->pin == '265478') {
            $device->status = 2;
            $device->pin = '';
            $device->save();
        } else {
            return response()->json(['error' => 'Código de validação inválido!'], 400);
        }
        
        $data = [
            "user_name" => $user->name,
            "user_cell" => $user->mobile,
            "user_email" => $user->email,
            "id_tipo_usuario" => 3,
            "panic" => $user->panic,
            "device" => $user->devices,           
            "condominios" => $user->condominiums->toArray(),
            "unidades" => $user->units->toArray(),
            "policy" => $user->policy
        ];

        return response()->json([
            "status" => "SUCCESS",
            "data" => $data
        ],200);
    }

    public function generatePIN($digits = 6)
    {
        $i = 0; //counter
        $pin = ""; //our default pin is blank.
        while($i < $digits){
            //generate a random number between 0 and 9.
            $pin .= mt_rand(0, 9);
            $i++;
        }
        return $pin;
    }

    public function vihuBoxInstall(Request $request) {
        $ip = $_SERVER['REMOTE_ADDR'];
        $uid = Carbon::now()->timestamp;

        //VERIFICA SE O IP EXISTE NA TABELA DE DNS
        $dns = DNS::where('ip', $ip)->first();
        if (!is_object($dns)) {
            Log::create(['uid'=>$uid, 'description' => 'IP não encontrado:' .$ip, 'type' => 'vihubox_install']);
            return response()->json(['error' => 'Acesso do condomínio não autorizado!'], 401);
        }

        //PEGA O CONDOMINIO DO IP SELECIONADO
        $condominium = Condominium::find($dns->condominiums_id);
        if (!is_object($condominium)) {
            Log::create(['uid'=>$uid, 'description' => 'Nenhum condominio encontrado para o ip:' .$ip, 'type' => 'vihubox_install']);
            return response()->json(['error' => 'Acesso do condomínio não autorizado!'], 401);
        }

        Config::set('jwt.user', 'App\Condominium'); 
        Config::set('auth.providers.users.model', \App\Condominium::class);

        try {
            $token = JWTAuth::attempt(['id'=>$condominium->id, 'password'=>$this->monstroDecrypt($request->password)]);
            if (!$token) {
                Log::create(['uid'=>$uid, 'description' => 'Login ou senha invalido', 'type' => 'vihubox_install']);
                return response()->json(['error' => 'Acesso do condomínio não autorizado!'], 401);
            }
        } catch (JWTException $e) {
            Log::create(['uid'=>$uid, 'description' => 'Não foi possível criar o token de acesso', 'type' => 'vihubox_install']);
            return response()->json(['error' => 'Não foi possível criar o token de acesso'], 500);
        }

        Log::create(['uid'=>$uid, 'description' => 'SUCESSO: '. $condominium->name, 'type' => 'vihubox_install']);
        return response()->json([
            "status" => "SUCCESS",
            "token"=> $token
        ], 200);        
    }

    private function monstroDecrypt($pwd) {
        // Remove the base64 encoding from our key
        $encryption_key = base64_decode(env('VIHUBOX_SECRET_KEY'));

        // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
        list($encrypted_data, $iv) = explode('::', base64_decode($pwd), 2);
        
        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
    }     
}
