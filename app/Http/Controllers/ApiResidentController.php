<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\Http\Controllers\Factory\UserFactory;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\UnitsUser;
use App\CondominiumsUsers;
use App\Http\Controllers\SMSController;
use App\Http\Controllers\VoiceController;

class ApiResidentController extends Controller implements CrudInterface
{
    public function index()
    {
        $allUsers = User::with(['units', 'units.condominium', 'types'])->get();

        $data = "Não há moradores para serem listados.";

        if ($allUsers->count() > 0 ) {
            $data = $allUsers->toArray();
        }

        return response()->json([
            "items" => $allUsers->count(),
            "users" => $data
        ],200);
    }

    public function create(Request $request)
    {
        $usuario = User::onlyTrashed()->where('email', $request->email)->first();

        if ($usuario) {
            $usuario->restore();
            $validator = Validator::make($request->all(), [
                "name" => "max:255",
                "email"  => "required|max:255",
                "password" => "required",
                "phone" => "max:255",
                "mobile" => "required|max:255",
                "panic" => "max:255"
            ]);
        } else {
            $usuario = new User;
            $validator = Validator::make($request->all(), [
                "name" => "max:255",
                "email"  => "required|max:255|unique:users",
                "password" => "required",
                "phone" => "max:255",
                "mobile" => "required|max:255|unique:users",
                "panic" => "max:255"
            ]);
        }

        if ($validator->fails()) {
            return  response()->json([
                "message" => "Não foi possível criar o usuário",
                "errors" => [
                    $validator->errors()
                ]
            ],400);
        }
    
        $usuario->name = $request->name;
        $usuario->password = bcrypt($request->password);
        $usuario->email = $request->email;
        $usuario->mobile = $request->mobile;
        $usuario->phone = $request->phone;
        $usuario->is_admin = $request->phone;
        $usuario->save();

        foreach($request->units as $unit) {
            UnitsUser::create([
                'units_id' => $unit['id'],
                'users_id' => $usuario->id,
                'type' => 'morador'
            ]);

            CondominiumsUsers::create([
                'condominiums_id' => $unit['condominiums_id'],
                'users_id' => $usuario->id,
                'type' => 'morador'
            ]);
        }        

        return response()->json([
            "message" => "Usuário criado com sucesso."
        ],201);
    }

    public function show($id)
    {
        $user = User::with('units')->find($id);

        if (is_object($user)) {
            return response()->json([
                "user" => $user
            ],200);
        }

        return  response()->json([
            "message" => "Usuário não encontrado",
        ],400);
    }

    public function patch(Request $request, $id) {}

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if (!is_object($user)) {
            return  response()->json([
                "message" => "Usuário não encontrado.",
            ],400);
        }

        if ($request->email != $user->email) {
            $validator = Validator::make($request->all(), [
                "email"  => "required|max:255|unique:users",
            ]);

            if ($validator->fails()) {
                return  response()->json([
                    "message" => "Não foi possível editar o usuário.",
                    "errors" => [
                        $validator->errors()
                    ]
                ],400);
            }
        }

        if ($request->mobile != $user->mobile) {
            $validator = Validator::make($request->all(), [
                "mobile" => "required|max:255|unique:users",
            ]);

            if ($validator->fails()) {
                return  response()->json([
                    "message" => "Não foi possível editar o usuário.",
                    "errors" => [
                        $validator->errors()
                    ]
                ],400);
            }
        }

        $validator = Validator::make($request->all(), [
            "name" => "max:255",
            "phone" => "max:255",
            "panic" => "max:255"
        ]);

        if ($validator->fails()) {
            return  response()->json([
                "message" => "Não foi possível editar o usuário.",
                "errors" => [
                    $validator->errors()
                ]
            ],400);
        }

        $user->name = $request->name;
        $user->email = $request->email;
        if (!empty($request->password)) {
            $user->password = bcrypt($request->password);
        }
        $user->phone = $request->phone;
        $user->mobile = $request->mobile;
        $user->save();

        UnitsUser::where('users_id', $id)->delete();
        CondominiumsUsers::where('users_id', $id)->delete();
        
        foreach($request->units as $unit) {
            UnitsUser::create([
                'units_id' => $unit['id'],
                'users_id' => $id,
                'type' => 'morador'
            ]);

            CondominiumsUsers::create([
                'condominiums_id' => $unit['condominiums_id'],
                'users_id' => $id,
                'type' => 'morador'
            ]);            
        }

        return  response()->json([
            "Message" => "Usuário alterado com sucesso."
        ],200);
    }

    public function delete($id)
    {
        $user = User::find($id);

        if (!is_object($user)) {
            return  response()->json([
                "message" => "usuário Não foi possível encontrado.",
            ],400);
        }

        if ($user->trashed()) {
            return  response()->json([
                "message" => "Usuário já deletado",
            ],400);
        }

        UnitsUser::where('users_id', $id)->delete();
        CondominiumsUsers::where('users_id', $id)->delete();

        $user->delete();

        return response()->json([
            "message" => "Usuário excluído com sucesso.",
        ],200);
    }

    public function validateUser(Request $request) {}

    public function concierge(Request $request, SMSController $sms)
    {
        $user = JWTAuth::toUser(JWTAuth::getToken());
        $concierge = CondominiumsUsers::where('condominiums_id', $request->condominioId)->where('type', 'zelador')->first();

        if ($concierge) {
            $result = $sms->sendToConcierge($user, $concierge->user);

            return response()->json([
                "message" => "Solicitação enviada com sucesso."
            ],200);                
        } else {
            return response()->json([
                "message" => "Não foi encontrado nenhum Zelador cadastrado para este condomínio."
            ],500);    
        }
    }

    public function changePassword(Request $request)
    {
        $user = JWTAuth::toUser(JWTAuth::getToken());

        $user->password = bcrypt($request->input('newPwd'));
        $user->save();

        return response()->json([
            "message" => "Senha alterada com sucesso."
        ],200);
    }

    public function panicSave(Request $request)
    {
        $user = JWTAuth::toUser(JWTAuth::getToken());

        $user->panic = $request->input('configs');
        $user->save();

        return response()->json([
            "message" => "Configurações salvas com sucesso."
        ],200);
    }

    public function panic(Request $request, VoiceController $voice)
    {
        $user = JWTAuth::toUser(JWTAuth::getToken());
        $result = $voice->sendToPanic($user, $request->all());

        return response()->json([
            "message" => "Solicitação enviada com sucesso."
        ],200);
    }

    public function safe(Request $request, SMSController $sms)
    {
        $user = JWTAuth::toUser(JWTAuth::getToken());
        $result = $sms->sendToSafe($user, $request->all());

        return response()->json([
            "message" => "Solicitação enviada com sucesso."
        ],200);
    }
}
