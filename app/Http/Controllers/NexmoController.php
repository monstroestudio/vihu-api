<?php
namespace App\Http\Controllers;

use Nexmo;
use Storage;
use App\Log;
use App\Voice;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class NexmoController extends Controller {

    private $basic;
    private $keypair;
    private $client;

    public function __construct() {
        $this->basic = new \Nexmo\Client\Credentials\Basic(env('NEXMO_KEY'), env('NEXMO_SECRET'));
        $this->keypair = new \Nexmo\Client\Credentials\Keypair(Storage::get('nexmo.key'), env('NEXMO_APPLICATION_ID'));
        $this->client = new \Nexmo\Client(new \Nexmo\Client\Credentials\Container($this->basic, $this->keypair));
    }

    public function sendURA($voice) {
        $call = $this->client->calls()->create([
            'to' => [[
                'type' => 'phone',
                'number' => '55'.$voice->mobile
            ]],
            'from' => [
                'type' => 'phone',
                'number' => '551136621552'
            ],
            'answer_url' => [env('APP_URL').'/api/ura/'.$voice->id],
            'event_url' => [env('APP_URL').'/api/ura/log']
        ]);

        return $call;
    }

    public function sendSMS($sms) {
        return Nexmo::message()->send([
            'type' => 'unicode',
            'to' => '55'.$sms->mobile,
            'from' => '551136621552',
            'text' => $sms->message
        ]);
    }

    public function answer(Request $request) {
        $voice = Voice::find($request->route('id'));

        $msg = [
            "action" => "talk",
            "voiceName" => "Vitoria",
            "text" => $voice->message
        ];

        return response()->json([$msg]);
    }
}