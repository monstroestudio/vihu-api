<?php

namespace App\Http\Controllers;

use JWTAuth;
use Hash;
use Carbon\Carbon;
use App\Http\Controllers\Factory\UserFactory;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Units;
use App\UnitsUser;
use App\CondominiumsUsers;
use App\Utils\GenerateCode;
use App\Http\Controllers\SMSController;
use App\Http\Controllers\VoiceController;

class ApiUserController extends Controller implements CrudInterface
{
    public function index()
    {
        $allUsers = User::with(['units', 'units.condominium', 'types'])->get();

        $data = "Não há usuários para serem listados.";

        if ($allUsers->count() > 0 ) {
            $data = $allUsers->toArray();
        }

        return response()->json([
            "items" => $allUsers->count(),
            "users" => $data
        ],200);
    }

    public function create(Request $request)
    {
        $usuario = User::onlyTrashed()->where('email', $request->email)->first();

        if ($usuario) {
            $usuario->restore();
            $validator = Validator::make($request->all(), [
                "name" => "max:255",
                "email"  => "required|max:255",
                "password" => "required",
                "phone" => "max:255",
                "mobile" => "required|max:255",
                "panic" => "max:255"
            ]);
        } else {
            $usuario = new User;
            $validator = Validator::make($request->all(), [
                "name" => "max:255",
                "email"  => "required|max:255|unique:users",
                "password" => "required",
                "phone" => "max:255",
                "mobile" => "required|max:255|unique:users",
                "panic" => "max:255"
            ]);
        }

        if ($validator->fails()) {
            return response()->json([
                "message" => ($validator->errors()->has('mobile')) ? "Este número de celular já existe" : "Não foi possível criar o usuário",
                "errors" => [
                    $validator->errors()
                ]
            ],400);
        }
    
        $usuario->name = $request->name;
        $usuario->password = bcrypt($request->password);
        $usuario->email = $request->email;
        $usuario->mobile = preg_replace("/[^0-9]/", "", $request->mobile);
        $usuario->phone = preg_replace("/[^0-9]/", "", $request->phone);
        $usuario->is_admin = $request->is_admin;
        $usuario->rfid_keys = $request->rfid_keys;
        $usuario->communication_type = $request->communication_type;
        $usuario->save();

        foreach($request->units as $unit) {
            UnitsUser::create([
                'units_id' => $unit['id'],
                'users_id' => $usuario->id,
                'type' => 'morador'
            ]);

            CondominiumsUsers::create([
                'condominiums_id' => $unit['condominiums_id'],
                'users_id' => $usuario->id,
                'type' => $unit['type']
            ]);
        }        

        return response()->json([
            "message" => "Usuário criado com sucesso."
        ], 201);
    }

    public function show($id)
    {
        $user = User::with('units')->with('types')->find($id);

        if (is_object($user)) {
            return response()->json([
                "user" => $user
            ],200);
        }

        return  response()->json([
            "message" => "Usuário não encontrado",
        ],400);
    }

    public function patch(Request $request, $id) {}

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if (!is_object($user)) {
            return  response()->json([
                "message" => "Usuário não encontrado.",
            ],400);
        }

        if ($request->email != $user->email) {
            $validator = Validator::make($request->all(), [
                "email"  => "required|max:255|unique:users",
            ]);

            if ($validator->fails()) {
                return  response()->json([
                    "message" => "Não foi possível editar o usuário.",
                    "errors" => [
                        $validator->errors()
                    ]
                ],400);
            }
        }

        if ($request->mobile != $user->mobile) {
            $validator = Validator::make($request->all(), [
                "mobile" => "required|max:255|unique:users",
            ]);

            if ($validator->fails()) {
                return  response()->json([
                    "message" => "Não foi possível editar o usuário.",
                    "errors" => [
                        $validator->errors()
                    ]
                ],400);
            }
        }

        $validator = Validator::make($request->all(), [
            "name" => "max:255",
            "phone" => "max:255",
            "panic" => "max:255"
        ]);

        if ($validator->fails()) {
            return  response()->json([
                "message" => "Não foi possível editar o usuário.",
                "errors" => [
                    $validator->errors()
                ]
            ],400);
        }

        $user->name = $request->name;
        $user->email = $request->email;
        if (!empty($request->password)) {
            $user->password = bcrypt($request->password);
        }
        $user->mobile = preg_replace("/[^0-9]/", "", $request->mobile);
        $user->phone = preg_replace("/[^0-9]/", "", $request->phone);
        $user->is_admin = $request->is_admin;
        $user->rfid_keys = $request->rfid_keys;
        $user->communication_type = $request->communication_type;
        $user->save();

        UnitsUser::where('users_id', $id)->delete();
        CondominiumsUsers::where('users_id', $id)->delete();
        
        foreach($request->units as $unit) {
            UnitsUser::create([
                'units_id' => $unit['id'],
                'users_id' => $id,
                'type' => 'morador'
            ]);

            CondominiumsUsers::create([
                'condominiums_id' => $unit['condominiums_id'],
                'users_id' => $id,
                'type' => $unit['type']
            ]);            
        }

        return  response()->json([
            "Message" => "Usuário alterado com sucesso."
        ],200);
    }

    public function delete($id)
    {
        $user = User::find($id);

        if (!is_object($user)) {
            return  response()->json([
                "message" => "usuário Não foi possível encontrado.",
            ],400);
        }

        if ($user->trashed()) {
            return  response()->json([
                "message" => "Usuário já deletado",
            ],400);
        }

        UnitsUser::where('users_id', $id)->delete();
        CondominiumsUsers::where('users_id', $id)->delete();

        $user->delete();

        return response()->json([
            "message" => "Usuário excluído com sucesso.",
        ],200);
    }

    public function validateUser(Request $request) {}

    public function concierge(Request $request, SMSController $sms)
    {
        $user = JWTAuth::toUser(JWTAuth::getToken());

        if ($request->condominioId != "") {
            $concierge = CondominiumsUsers::where('condominiums_id', $request->condominioId)->where('type', 'zelador')->first();
        } else {
            $concierge = CondominiumsUsers::where('condominiums_id', $user->condominiums->first()->id)->where('type', 'zelador')->first();
        }        

        if ($concierge) {
            $result = $sms->sendToConcierge($user, $concierge->user);

            return response()->json([
                "message" => "Solicitação enviada com sucesso."
            ],200);                
        } else {
            return response()->json([
                "message" => "Não foi encontrado nenhum Zelador cadastrado para este condomínio."
            ],500);    
        }
    }

    public function changePassword(Request $request)
    {
        $user = JWTAuth::toUser(JWTAuth::getToken());

        if (!Hash::check($request->input('pwd'), $user->password)) {
            return response()->json([
                "message" => "Senha atual inválida."
            ],400);            
        }        

        $user->password = bcrypt($request->input('newPwd'));
        $user->save();

        return response()->json([
            "message" => "Senha alterada com sucesso."
        ],200);
    }

    public function panicSave(Request $request)
    {
        $user = JWTAuth::toUser(JWTAuth::getToken());

        $user->panic = $request->input('configs');
        $user->save();

        return response()->json([
            "message" => "Configurações salvas com sucesso."
        ],200);
    }

    public function panic(Request $request, VoiceController $voice, SMSController $sms)
    {
        $user = JWTAuth::toUser(JWTAuth::getToken());
        $resultSMS = $sms->sendToPanic($user, $request->all());
        $resultVoice = $voice->sendToPanic($user, $request->all());

        return response()->json([
            "message" => "Solicitação enviada com sucesso."
        ],200);
    }

    public function safe(Request $request, SMSController $sms)
    {
        $user = JWTAuth::toUser(JWTAuth::getToken());
        $result = $sms->sendToSafe($user, $request->all());

        return response()->json([
            "message" => "Solicitação enviada com sucesso."
        ],200);
    }

    public function policy(Request $request)
    {
        $user = JWTAuth::toUser(JWTAuth::getToken());

        $user->policy = $request->input('policy');
        $user->save();

        return response()->json([
            "message" => "Política salva com sucesso."
        ],200);
    }

    public function usersToUnit(Request $request) 
    {
        $unitID = $request->route('unit');
        $unit = Units::find($unitID);

        $users = User::all();

        foreach($users as $user) {
            UnitsUser::create([
                'units_id' => $unit->id,
                'users_id' => $user->id,
                'type' => 'morador'
            ]);

            CondominiumsUsers::create([
                'condominiums_id' => $unit->condominiums_id,
                'users_id' => $user->id,
                'type' => 'morador'
            ]);
        }        
    }

    public function validatePassword(Request $request)
    {
        $user = JWTAuth::toUser(JWTAuth::getToken());    
        
        if (!Hash::check($request->input('password'), $user->password)) {
            return response()->json([
                "message" => "Senha inválida."
            ],400);
        }        

        return response()->json([
            "message" => "senha válida."
        ],200);
    }


    public function generateAccessCode(Request $request)
    {
        $user = JWTAuth::toUser(JWTAuth::getToken());       
        $condominiums = [];

        foreach ($user->condominiums->toArray() as $condominium) {
            array_push($condominiums, $condominium['id']);
        }

        $code = GenerateCode::resident($condominiums);

        $user->access_code = sha1($code);
        $user->access_code_date = Carbon::now()->toDateTimeString();
        $user->save();

        return response()->json([
            "message" => "Código de acesso gerado com sucesso.",
            "accessCode" => $code
        ],200);
    }
}
