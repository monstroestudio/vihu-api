<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\SMSController;
use Carbon\Carbon;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PasswordController extends Controller
{
    public function reset(Request $request, SMSController $sms) {
        $user = User::where('mobile', $request->input('mobile'))->first();

        if (!$user) {
            return response()->json(['error' => 'user_not_found', 'message' => 'Número de celular não encontrado'], 400);
        }

        //GERA PIN
        $pin = $this->generatePIN();

        //SALVA PIN NO BANCO DE DADOS
        $user->remember_pin = $pin;
        $user->save();

        //ENVIA PIN POR SMS
        $sms->sendToValidation($user, $pin, 'código VIHU para resetar sua senha.');

        return response()->json([
            "status" => "SUCCESS",
            "data" => [
                "user_id" => $user->id,
                "user_name" => $user->name,
                "user_cell" => $user->mobile,
                "user_email" => $user->email
            ]
        ], 200);
    }

    public function pin(Request $request) {
        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(['error' => 'user_not_found', 'message' => 'Usuário não encontrado.'], 401);
        }

        if ($user->remember_pin != $request->pin) {
            return response()->json(['error' => 'invalid_pin', 'message' => 'PIN inválido ou expirado'], 400);
        }

        $token = Hash::make(Carbon::now()->timestamp);
        $user->remember_pin = NULL;
        $user->remember_token = $token;
        $user->save();

        return response()->json([
            "status" => "SUCCESS",
            "data" => [
                "user_id" => $user->id,
                "user_name" => $user->name,
                "user_cell" => $user->mobile,
                "user_email" => $user->email,
                "token" => $token
            ]
        ], 200);    
    }

    public function save(Request $request) {
        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(['error' => 'user_not_found', 'message' => 'Usuário não encontrado.'], 401);
        }

        if ($user->remember_token != $request->token) {
            return response()->json(['error' => 'invalid_token', 'message' => 'Não foi possível cadastrar sua senha. Por favor, tente novamente.'], 401);
        }

        $validator = Validator::make($request->all(), [
            "password" => "required"
        ]);

        if ($validator->fails()) {
            return response()->json([
                "message" => "Não foi possível salvar sua nova senha",
                "errors" => [
                    $validator->errors()
                ]
            ],400);
        }

        $user->password = bcrypt($request->password);;
        $user->remember_token = NULL;
        $user->save();

        return response()->json([
            "status" => "SUCCESS",
            "data" => [
                "user_id" => $user->id,
                "user_name" => $user->name,
                "user_cell" => $user->mobile,
                "user_email" => $user->email
            ]
        ], 200);    
    }

    public function generatePIN($digits = 6)
    {
        $i = 0; //counter
        $pin = ""; //our default pin is blank.
        while($i < $digits){
            //generate a random number between 0 and 9.
            $pin .= mt_rand(0, 9);
            $i++;
        }
        return $pin;
    }
}
