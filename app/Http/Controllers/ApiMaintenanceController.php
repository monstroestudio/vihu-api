<?php

namespace App\Http\Controllers;

use App\Access;
use App\DNS;
use App\Condominium;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ApiMaintenanceController extends Controller
{
    public function maintenance() {        
        $accesses = Access::where('status', 'ativo')->whereDate('date', '<', Carbon::yesterday()->toDateString())->update(['status' => 3]);
        
        return response()->json([
            "access" => $accesses . " atualizados."
        ],200);
    }

    public function migrateDNS(){
        $condominiums = Condominium::all();
        foreach ($condominiums as $condominium) {
            if ($condominium->dns != '') {
                DNS::create([
                    'condominiums_id' => $condominium->id,
                    'dns' => $condominium->dns,
                    'ip' => $condominium->ip
                ]);
            }
        }
    }
}
