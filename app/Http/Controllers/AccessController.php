<?php

namespace App\Http\Controllers;

use Config;
use Hash;
use JWTAuth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Condominium;
use App\Log;
use App\DNS;
use App\LogAccess;
use App\LogResident;
use App\User;
use App\UnitsUser;
use App\Units;
use App\Access;
use App\Utils\HourValidator;
use App\Utils\GenerateCode;
use App\Http\Controllers\SMSController;
use App\Http\Controllers\VoiceController;
use Tymon\JWTAuth\Exceptions\JWTException;

class AccessController extends Controller
{
    public function syncDown(Request $request)
    {
        $uid = Carbon::now()->timestamp . '.' . rand();
        $ip = $_SERVER['REMOTE_ADDR'];
        $acessos = ['moradores' => null, 'visitantes' => null ];
        
        Log::create(['uid'=>$uid, 'description' => 'Inicio', 'type' => 'syncDown', 'ip' => $ip]);
        
        Config::set('jwt.user', 'App\Condominium'); 
        Config::set('auth.providers.users.model', \App\Condominium::class);       
        
        //PEGA O CONDOMINIO ATRAVES DO TOKEN
        $condominium = JWTAuth::toUser(JWTAuth::getToken());

        if (!is_object($condominium)) {
            Log::create(['uid'=>$uid, 'description' => 'Token inválido: ' . $ip, 'type' => 'syncDown', 'ip' => $ip]);
            return response()->json([
                "message" => "Não autorizado."
            ], 401);
        }

        //PEGA OS ACESSOS DO CONDOMINIO
        $acessos['condominio'] = $condominium->toArray();
        //Log::create(['uid'=>$uid, 'description' => 'Guardou dados do condominio', 'type' => 'syncDown', 'ip' => $ip]);

        $acessos['visitantes'] = Access::where('status', 'ativo')->where('condominiums_id', $condominium->id)->get()->toArray();
        //Log::create(['uid'=>$uid, 'description' => 'Guardou acesso dos visitantes', 'type' => 'syncDown', 'ip' => $ip]);

        $acessos['moradores'] = User::whereHas('condominiums', function ($query) use ($condominium) {
            $query->where('id', $condominium->id);
        })->with(['units' => function($query) use ($condominium){
            $query->select('number','extension_number')->where('condominiums_id', $condominium->id);
        }])->get()->makeVisible('access_code')->toArray();

        Log::create(['uid'=>$uid, 'description' => 'Sync OK - '.$condominium->name.': ' . count($acessos['moradores']) . '|' . count($acessos['visitantes']), 'type' => 'syncDown', 'ip' => $ip]);
        
        return response()->json([
            "data" => $acessos
        ], 200);
    }

    public function syncUp(Request $request)
    {
        $updated = 0;
        $notFound = 0;
        $ip = $_SERVER['REMOTE_ADDR'];
        $codes = json_decode($request->getContent());
        $uid = Carbon::now()->timestamp . '.' . rand();

        Config::set('jwt.user', 'App\Condominium'); 
        Config::set('auth.providers.users.model', \App\Condominium::class);       
        
        //PEGA O CONDOMINIO ATRAVES DO TOKEN
        $condominium = JWTAuth::toUser(JWTAuth::getToken());

        if (!is_object($condominium)) {
            Log::create(['uid'=>$uid, 'description' => 'Token inválido: ' . $ip, 'type' => 'syncUp', 'ip' => $ip]);
            return response()->json([
                "message" => "Não autorizado."
            ], 401);
        }        
        
        if ($codes != '') {
            foreach ($codes->accesses as $item) {
                $acesso = Access::find($item->id);

                if ($acesso) {
                    $acesso->status = $item->status;
                    $acesso->validations = $item->validations;
                    $acesso->gates = json_encode($item->gates);
                    $acesso->save();

                    $updated++;
                } else {
                    $notFound++;
                }
            }

            if ($codes->logAccess) {
                foreach ($codes->logAccess as $log) {
                    LogResident::create([
                        'user_id' => $log->user_id,
                        'condominiums_id' => $log->condominiums_id,
                        'keyboard' => $log->keyboard,
                        'type' => $log->type,
                        'ip' => $log->ip
                    ]);
                }
            }
        }

        

        Log::create(['uid'=>$uid, 'description' => 'Resultado da sincronização: updated:' . $updated . ' | not_found:' . $notFound, 'type' => 'syncUp', 'ip' => $ip]);
        
        return response()->json([
            "updated" => $updated,
            "not_found" => $notFound
        ], 200);      
    }

    public function access(Request $request, VoiceController $voice, SMSController $sms)
    {
        $uid = Carbon::now()->timestamp;
        $ip = $_SERVER['REMOTE_ADDR'];
        $code = $request->route('code');
        $keyboard = $request->route('keyboard');

        Log::create(['uid'=>$uid, 'description' => '01 - ' . $code, 'type' => 'access', 'ip' => $ip]);

        Config::set('jwt.user', 'App\Condominium'); 
        Config::set('auth.providers.users.model', \App\Condominium::class);       
        
        //PEGA O CONDOMINIO ATRAVES DO TOKEN
        $condominium = JWTAuth::toUser(JWTAuth::getToken());

        if (!is_object($condominium)) {
            LogAccess::create([
                'access_id' => NULL,
                'condominiums_id' => NULL,
                'code' => $code,
                'keyboard' => $keyboard,
                'ip' => $ip,
                'status' => 4
            ]);
            return $this->resultEncode(json_encode(["type" => "ACESSO_INVALIDO"]));
        }        
        
        Log::create(['uid'=>$uid, 'description' => '02 - ' . $code, 'type' => 'access', 'ip' => $ip]);
        
        //VERIFICA SE A SENHA É 123456 E RETORNA INVALIDO
        if ($code == '123456') {
            LogAccess::create([
                'access_id' => NULL,
                'condominiums_id' => $condominium->id,
                'code' => $code,
                'keyboard' => $keyboard,
                'ip' => $ip,
                'status' => 2
            ]);

            Log::create(['uid'=>$uid, 'description' => '03 | SENHA FRACA | ' . $code, 'type' => 'access', 'ip' => $ip]);

            return $this->resultEncode(json_encode(["type" => "ACESSO_INVALIDO"]));
        }

        Log::create(['uid'=>$uid, 'description' => '04 - ' . $code, 'type' => 'access', 'ip' => $ip]);

        //VERIFICA SE O CODIGO EXISTE PARA O MORADOR DO CONDOMINIO QUE SOLICITOU A VALIDAÇÃO
        $morador = User::where('access_code', $code)->whereHas('condominiums', function ($query) use ($condominium) {
            $query->where('id', $condominium->id);
        })->first();
        
        if (is_object($morador)) {
            LogResident::create([
                'user_id' => $morador->id,
                'condominiums_id' => $condominium->id,
                'keyboard' => $keyboard,
                'ip' => $ip
            ]);

            Log::create(['uid'=>$uid, 'description' => '06 | MORADOR | ACESSO_LIBERADO | ' . $code, 'type' => 'access', 'ip' => $ip]);

            return $this->resultEncode(json_encode(["type" => "ACESSO_LIBERADO"]));
        }
        
        Log::create(['uid'=>$uid, 'description' => '07 - ' . $code, 'type' => 'access', 'ip' => $ip]);
        
        //VERIFICA SE O CODIGO EXISTE PARA O CONDOMINIO QUE SOLICITOU A VALIDAÇÃO
        $acesso = Access::where('code', '=', $code)->where('status', 'ativo')->where('condominiums_id', $condominium->id)->with('condominium')->first();      
        if (!is_object($acesso)) {
            LogAccess::create([
                'access_id' => NULL,
                'condominiums_id' => $condominium->id,
                'code' => $code,
                'keyboard' => $keyboard,
                'ip' => $ip,
                'status' => 2
            ]);

            Log::create(['uid'=>$uid, 'description' => '08 | ACESSO NÃO EXISTE PARA CONDOMINIO | ' . $code, 'type' => 'access', 'ip' => $ip]);

            return $this->resultEncode(json_encode(["type" => "ACESSO_INVALIDO"]));
        }

        Log::create(['uid'=>$uid, 'description' => '09 - ' . $code, 'type' => 'access', 'ip' => $ip]);

        //VERIFICA SE O CODIGO JA FOI DIGITADO NESSE TECLADO
        if ($keyboard && $acesso->gates) {
            $_gates = json_decode($acesso->gates, true);

            foreach ($_gates as $gate) {
                if ($gate['gate'] == $keyboard) {
                    LogAccess::create([
                        'access_id' => NULL,
                        'condominiums_id' => $condominium->id,
                        'code' => $code,
                        'keyboard' => $keyboard,
                        'ip' => $ip,
                        'status' => 5
                    ]);

                    Log::create(['uid'=>$uid, 'description' => '10 | PORTÃO INVALIDO | ' . $code, 'type' => 'access', 'ip' => $ip]);
        
                    return $this->resultEncode(json_encode(["type" => "ACESSO_INVALIDO"]));
                }
            }
        }

        Log::create(['uid'=>$uid, 'description' => '11 - ' . $code, 'type' => 'access', 'ip' => $ip]);

        $horario = new HourValidator($acesso);
        if ($horario->isValid()) {

            Log::create(['uid'=>$uid, 'description' => '12 | VALIDOU HORARIO | ' . $code, 'type' => 'access', 'ip' => $ip]);

            $user = User::with(['units' => function($query) use ($condominium){
                $query->select('number','extension_number')->where('condominiums_id', $condominium->id);
            }])->where('id', $acesso->users_id)->first();

            $gate = [
                'gate' => $keyboard,
                'datetime' => Carbon::now()->toDateTimeString()
            ];
            
            if ((($acesso->type == 'delivery') || ($acesso->validations == 0)) && (!$acesso->exit) ) {
                $voice->sendToDweller($acesso);
                $sms->sendToDweller($acesso);
            }

            if ($acesso->type == 'delivery') {
                $acesso->status = 2;
                $acesso->validations = 1;
                $acesso->gates = json_encode([$gate]);
                $acesso->save();

                LogAccess::create([
                    'access_id' => $acesso->id,
                    'condominiums_id' => $condominium->id,
                    'code' => $code,
                    'keyboard' => $keyboard,
                    'ip' => $ip,
                    'status' => 1
                ]);

                Log::create(['uid'=>$uid, 'description' => '13 | DELIVERY | ACESSO_LIBERADO | ' . $code, 'type' => 'access', 'ip' => $ip]);

                return $this->resultEncode(json_encode(["type" => "DELIVERY", "ramal" => $user->units[0]->extension_number]));
            } else {
                if (($acesso->condominium->number_of_gates == $acesso->validations + 1) || ($keyboard == $acesso->condominium->number_of_gates) || ($acesso->exit)) {
                    $acesso->status = 2;
                }

                if ($acesso->gates) {
                    $_gates = json_decode($acesso->gates, true);
                    array_push($_gates, $gate);
                    $acesso->gates = json_encode($_gates);
                } else {
                    $acesso->gates = json_encode([$gate]);
                }

                $acesso->validations = $acesso->validations + 1;
                $acesso->save();

                if ($acesso->type == 'visitante' || $acesso->type == 'festa') {
                    LogAccess::create([
                        'access_id' => $acesso->id,
                        'condominiums_id' => $condominium->id,
                        'code' => $code,
                        'keyboard' => $keyboard,
                        'ip' => $ip,
                        'status' => 1
                    ]);

                    Log::create(['uid'=>$uid, 'description' => '14 | VISITA | ACESSO_LIBERADO | ' . $code, 'type' => 'access', 'ip' => $ip]);

                    if ($acesso->validations > 1){
                        return $this->resultEncode(json_encode(["type" => "ACESSO_LIBERADO", "ramal" => $user->units[0]->extension_number]));
                    } else {
                        return $this->resultEncode(json_encode(["type" => "ACESSO_VISITANTE", "ramal" => $user->units[0]->extension_number]));
                    }
                }
            }
        }

        LogAccess::create([
            'access_id' => $acesso->id,
            'condominiums_id' => $condominium->id,
            'code' => $code,
            'keyboard' => $keyboard,
            'ip' => $ip,
            'status' => 3
        ]);

        Log::create(['uid'=>$uid, 'description' => '15 - | ACESSO_INVALIDO | ' . $code, 'type' => 'access', 'ip' => $ip]);

        return $this->resultEncode(self::INVALIDO);
    }

    public function accessByKey(Request $request)
    {
        $uid = Carbon::now()->timestamp;
        $ip = $_SERVER['REMOTE_ADDR'];
        $code = $request->route('code');
        $keyboard = $request->route('keyboard');

        Log::create(['uid'=>$uid, 'description' => '1 - ' . $code, 'type' => 'general', 'ip' => $ip]);

        //VERIFICA SE O IP EXISTE NA TABELA DE DNS
        $dns = DNS::where('ip', $ip)->first();        
        if (!is_object($dns)) {
            LogAccess::create([
                'access_id' => NULL,
                'condominiums_id' => NULL,
                'code' => $code,
                'ip' => $ip,
                'status' => 4
            ]);

            return $this->resultEncode(self::INVALIDO);
        }

        // Log::create(['uid'=>$uid, 'description' => '2 - ' . $code, 'type' => 'general', 'ip' => $ip]);
        
        // //PEGA O CONDOMINIO DO IP SELECIONADO
        $condominium = Condominium::find($dns->condominiums_id);

        //VERIFICA SE O CODIGO EXISTE PARA O MORADOR DO CONDOMINIO QUE SOLICITOU A VALIDAÇÃO
        $moradores = User::whereHas('condominiums', function ($query) use ($condominium) {
            $query->where('id', $condominium->id);
        })->whereRaw('JSON_CONTAINS(rfid_keys, \'{"code":"123"}\')')->get();
        foreach ($moradores as $morador) {
            if (Hash::check($code, $morador->password)) {

                LogResident::create([
                    'user_id' => $morador->id,
                    'condominiums_id' => $condominium->id,
                    'keyboard' => $keyboard,
                    'ip' => $ip
                ]);        

                return base64_encode("python /var/www/vihu/py/portao_l.py");
            }
        }

        dd($moradores);

        Log::create(['uid'=>$uid, 'description' => '3 - ' . $code, 'type' => 'general', 'ip' => $ip]);
        
        //VERIFICA SE O CODIGO EXISTE PARA O CONDOMINIO QUE SOLICITOU A VALIDAÇÃO
        $acesso = Access::where('code', '=', $code)->where('status', 'ativo')->where('condominiums_id', $condominium->id)->with('condominium')->first();      
        if (!is_object($acesso)) {
            LogAccess::create([
                'access_id' => NULL,
                'condominiums_id' => $condominium->id,
                'code' => $code,
                'ip' => $ip,
                'status' => 2
            ]);

            return $this->resultEncode(self::INVALIDO);
        }

        //VERIFICA SE O CODIGO JA FOI DIGITADO NESSE TECLADO
        if ($keyboard && $acesso->gates) {
            $_gates = json_decode($acesso->gates, true);
            foreach ($_gates as $gate) {
                if ($gate['gate'] == $keyboard) {
                    LogAccess::create([
                        'access_id' => NULL,
                        'condominiums_id' => $condominium->id,
                        'code' => $code,
                        'ip' => $ip,
                        'status' => 5
                    ]);
        
                    return $this->resultEncode(self::INVALIDO);
                }
            }
        }

        Log::create(['uid'=>$uid, 'description' => '4 - ' . $code, 'type' => 'general', 'ip' => $ip]);

        $horario = new HourValidator($acesso);
        if ($horario->isValid()) {

            $gate = [
                'gate' => $keyboard,
                'datetime' => Carbon::now()->toDateTimeString()
            ];
            
            if ((($acesso->type == 'delivery') || ($acesso->validations == 0)) && (!$acesso->exit) ) {
                $voice->sendToDweller($acesso);
                $sms->sendToDweller($acesso);
            }

            if ($acesso->type == 'delivery') {
                $acesso->status = 2;
                $acesso->validations = 1;
                $acesso->gates = json_encode([$gate]);
                $acesso->save();

                LogAccess::create([
                    'access_id' => $acesso->id,
                    'condominiums_id' => $condominium->id,
                    'code' => $code,
                    'ip' => $ip,
                    'status' => 1
                ]);

                return $this->resultEncode(self::DELIVERY);
            } else {
                if (($acesso->condominium->number_of_gates == $acesso->validations + 1) || ($keyboard == $acesso->condominium->number_of_gates) || ($acesso->exit)) {
                    $acesso->status = 2;
                }

                if ($acesso->gates) {
                    $_gates = json_decode($acesso->gates, true);
                    array_push($_gates, $gate);
                    $acesso->gates = json_encode($_gates);
                } else {
                    $acesso->gates = json_encode([$gate]);
                }

                $acesso->validations = $acesso->validations + 1;
                $acesso->save();

                if ($acesso->type == 'visitante') {
                    LogAccess::create([
                        'access_id' => $acesso->id,
                        'condominiums_id' => $condominium->id,
                        'code' => $code,
                        'ip' => $ip,
                        'status' => 1
                    ]);

                    return ($acesso->validations > 1 || $acesso->exit) ? $this->resultEncode(self::PORTAO1) : $this->resultEncode(self::PORTAO2);
                } else if ($acesso->type == 'festa') {
                    LogAccess::create([
                        'access_id' => $acesso->id,
                        'condominiums_id' => $condominium->id,
                        'code' => $code,
                        'ip' => $ip,
                        'status' => 1
                    ]);

                    return ($acesso->validations > 1 || $acesso->exit) ? $this->resultEncode(self::PORTAO1) : $this->resultEncode(self::PORTAO2);
                }
            }
        }

        LogAccess::create([
            'access_id' => $acesso->id,
            'condominiums_id' => $condominium->id,
            'code' => $code,
            'ip' => $ip,
            'status' => 3
        ]);

        return $this->resultEncode(self::INVALIDO);
    }

    public function create(Request $request, SMSController $sms)
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $uid = Carbon::now()->timestamp;
        $user = JWTAuth::toUser(JWTAuth::getToken());
        $unit = null;

        Log::create(['uid'=>$uid, 'description' => 'CREATE ACCESS 1: ' . $user->toJSON(), 'type' => 'create_access']);
        
        if ($user->units()->count() == 0) {
            return response()->json([
                "message" => "Não foi possível criar acesso para o condomínio informado."
            ],401);
        } else {        
            foreach ($user->units()->get() as $_unit) {
                if ($request->condominioId == $_unit->condominiums_id) {
                    $unit = $_unit;
                }
            }
            if (!$unit) {
                return response()->json([
                    "message" => "Não foi possível criar acesso para o condomínio informado."
                ],401);                    
            }
        }
        
        Log::create(['uid'=>$uid, 'description' => 'CREATE ACCESS 2: ' . $user->units()->get()->toJSON(), 'type' => 'create_access']);

        $code = GenerateCode::visit($unit->condominiums_id);

        Log::create(['uid'=>$uid, 'description' => 'CREATE ACCESS 3: ' . $code, 'type' => 'create_access']);

        $access = new Access;
        $access->users_id = $user->id;
        $access->visitor_name = ($request->tipo == 2) ? $user->name : $request->name;
        $access->visitor_mobile = ($request->tipo == 2) ? $user->mobile : $request->celular;
        $access->condominiums_id = $request->condominioId;
        $access->date = Carbon::createFromFormat('Y-m-d', $request->dataIni)->toDateString();
        $access->time = $request->horaIni;
        $access->code = $code;
        $access->type = $request->tipo;
        $access->status = 'ATIVO';
        $access->exit = ($request->onlyExiting == 'S') ? 1 : 0;
        $access->save();

        Log::create(['uid'=>$uid, 'description' => 'CREATE ACCESS 4: ' . $access->toJSON(), 'type' => 'create_access']);

        if ($request->tipo == 2) {
            Log::create(['uid'=>$uid, 'description' => 'CREATE ACCESS 5: DELIVERY OK', 'type' => 'create_access']);

            return response()->json([
                "message" => 'Solicitação realizada com sucesso!<br>Sua senha é <strong>' . $code . '</strong>',
                "code" => "$code"
            ],200);
        } else {
            Log::create(['uid'=>$uid, 'description' => 'CREATE ACCESS 5: PREPARE TO SEND SMS', 'type' => 'create_access']);

            $sms->sendToAccess($access);
            
            Log::create(['uid'=>$uid, 'description' => 'CREATE ACCESS 6: ACESSO OK', 'type' => 'create_access']);

            return response()->json([
                "message" => "Acesso enviado com sucesso.",
                "code" => "$code"
            ],200);
        }
    }

    public function party(Request $request, SMSController $sms)
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $uid = Carbon::now()->timestamp;
        $user = JWTAuth::toUser(JWTAuth::getToken());
        $unit = null;

        Log::create(['uid'=>$uid, 'description' => 'PARTY 1: ' . $user->toJSON(), 'type' => 'party', 'ip' => $ip]);

        if ($user->units()->count() == 0) {
            return response()->json([
                "message" => "Não foi possível criar acesso para o condomínio informado."
            ],401);
        } else {        
            foreach ($user->units()->get() as $_unit) {
                if ($request->condominioId == $_unit->condominiums_id) {
                    $unit = $_unit;
                }
            }
            if (!$unit) {
                return response()->json([
                    "message" => "Não foi possível criar acesso para o condomínio informado."
                ],401);                    
            }
        }

        Log::create(['uid'=>$uid, 'description' => 'PARTY 2: ' . $user->units()->get()->toJSON(), 'type' => 'party', 'ip' => $ip]);

        foreach ($request->visitors as $visitor) {
            $code = new GenerateCode($unit->condominiums_id);

            Log::create(['uid'=>$uid, 'description' => 'PARTY 3: ' . $code, 'type' => 'party', 'ip' => $ip]);

            $access = new Access;
            $access->users_id = $user->id;
            $access->visitor_name = $visitor['nome'];
            $access->visitor_mobile = $visitor['celular'];
            $access->condominiums_id = $request->condominioId;
            $access->date = Carbon::createFromFormat('Y-m-d', $request->dataIni)->toDateString();
            $access->time = $request->horaIni;
            $access->code = $code;
            $access->type = 3;
            $access->status = 'ATIVO';
            $access->save();

            Log::create(['uid'=>$uid, 'description' => 'PARTY 4: ' . $access->toJSON(), 'type' => 'party', 'ip' => $ip]);
            
            $sms->sendToAccess($access);
        }

        Log::create(['uid'=>$uid, 'description' => 'PARTY 4: COMPLETED', 'type' => 'party', 'ip' => $ip]);
            
        return response()->json([
            "message" => "Acessos enviados com sucesso."
        ],200);
    }

    private function resultEncode($content) {
        return base64_encode($content);
    }
}