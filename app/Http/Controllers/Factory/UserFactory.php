<?php

namespace App\Http\Controllers\Factory;

use Validator;
use App\User;

class UserFactory
{
    /**
     * @param array $data
     */
    static public function create(Array $data)
    {
        $validator = Validator::make($data, [
            "name" => "max:255",
            "email"  => "required|max:255|unique:users",
            "password" => "required",
            "phone" => "max:255",
            "mobile" => "required|max:255|unique:users",
            "panic" => "max:255"
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }

        $usuario = new User;

        $usuario->name = $data['name'];
        $usuario->password = encrypt($data['password']);
        $usuario->email = $data['email'];
        $usuario->mobile = $data['mobile'];
        $usuario->phone = $data['phone'];
        $usuario->panic = $data['panic'];

        $usuario->save();
    }
}