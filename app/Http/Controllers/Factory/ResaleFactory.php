<?php

namespace App\Http\Controllers\Factory;

use Validator;
use App\Resale;

class ResaleFactory {

    /**
     * @param array $data
     */
    static public function create(Array $data)
    {
        $validator = Validator::make($data, [
            'social_name' => 'required|max:255',
            'name' => 'required',
            'cnpj' => 'required|unique:resale',
            'address' => 'required|max:100',
            'number' => 'required',
            'district' => 'max:50',
            'city' => 'required|max:100',
            'state' => 'required|max:100',
            'cep' => 'required|max:100',
            'country' => 'max:100',
            'site' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }

        $resale = new Resale;

        $resale->social_name = $data["social_name"];
        $resale->name = $data["name"];
        $resale->cnpj = $data["cnpj"];
        $resale->address = $data["address"];
        $resale->number = $data["number"];
        $resale->complement = $data["complement"];
        $resale->district = $data["district"];
        $resale->city = $data["city"];
        $resale->state = $data["state"];
        $resale->cep = $data["cep"];
        $resale->country = $data["country"];
        $resale->site = $data["site"];
        $resale->status = "ATIVO";
        return $resale->save();
    }
}