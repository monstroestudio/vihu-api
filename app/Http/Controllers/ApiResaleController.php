<?php

namespace App\Http\Controllers;

use App\Condominium;
use Validator;
use DB;
use App\Resale;
use Illuminate\Http\Request;
use App\Http\Controllers\Factory\ResaleFactory;

class ApiResaleController extends Controller implements CrudInterface
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $allResales = Resale::all();
        $data = "Não há revendas para serem listadas.";

        if ($allResales->count() > 0) {
            $data = [];
            foreach ($allResales as $resale) {
                $arr = $resale->toArray();
                $data[] = $arr;
            }
        }

        return response()->json([
            "items" => $allResales->count(),
            "resales" => $data
        ],200);
    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        $resale = ResaleFactory::create($request->all());
        return $this->formatedResponse($resale, 'add','Revenda');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $resale = Resale::find($id);

        if (is_object($resale)) {
            return response()->json([
                "resale" => $resale
            ],200);
        }

        return  response()->json([
            "Message" => "Revenda não encontrada.",
        ],400);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $resale = Resale::find($id);

        if (!is_object($resale)) {
            return  response()->json([
                "message" => "Condomínio não encontrado",
            ],400);
        }

        if ($resale->cnpj != $request->cnpj) {
            $validator = Validator::make($request->all(), [
                'cnpj' => 'required|unique:resale',
            ]);

            if ($validator->fails()) {
                return  response()->json([
                    "message" => "Não foi possível editar a revenda.",
                    "errors" => [
                        $validator->errors()
                    ]
                ],400);
            }
        }

        $validator = Validator::make($request->all(), [
            'social_name' => 'required|max:255',
            'name' => 'required',
            'address' => 'required|max:100',
            'number' => 'required',
            'district' => 'max:50',
            'city' => 'required|max:100',
            'state' => 'required|max:100',
            'cep' => 'required|max:100',
            'country' => 'max:100',
            'site' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return  response()->json([
                "message" => "Não foi possível editar a revenda.",
                "errors" => [
                    $validator->errors()
                ]
            ],400);
        }

        $resale->social_name = $request->social_name;
        $resale->name = $request->name;
        $resale->cnpj = $request->cnpj;
        $resale->address = $request->address;
        $resale->number = $request->number;
        $resale->complement = $request->complement;
        $resale->district = $request->district;
        $resale->city = $request->city;
        $resale->state = $request->state;
        $resale->cep = $request->cep;
        $resale->country = $request->country;
        $resale->site = $request->site;
        $resale->status = "ATIVO";
        $resale->save();

        return  response()->json([
            "message" => "Revenda alterada com sucesso."
        ],200);
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function patch(Request $request, $id)
    {}

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $resale = Resale::find($id);

        if (!is_object($resale)) {
            return  response()->json([
                "message" => "Revenda não encontrada.",
            ],400);
        }

        if ($resale->condominiums->count() > 0 ) {
            return  response()->json([
                "message" => "Não é possível excluir a revenda pois existem condomínios vinculados a ela.",
            ],400);
        }

        $resale->status = 2;
        $resale->save();
        $resale->delete();

        return  response()->json([
            "message" => "Revenda excluída com sucesso."
        ],200);
    }
}
