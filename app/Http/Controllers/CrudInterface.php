<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

interface CrudInterface
{
    public function index();
    public function create(Request $request);
    public function show($id);
    public function update(Request $request, $id);
    public function patch(Request $request, $id);
    public function delete($id);
}