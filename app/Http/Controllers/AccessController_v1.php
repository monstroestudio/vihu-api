<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Hash;
use JWTAuth;
use Illuminate\Http\Request;
use App\Condominium;
use App\Log;
use App\DNS;
use App\LogAccess;
use App\LogResident;
use App\User;
use App\UnitsUser;
use App\Units;
use App\Access;
use App\Utils\HourValidator;
use App\Http\Controllers\SMSController;
use App\Http\Controllers\VoiceController;

class AccessController_v1 extends Controller
{
    const DELIVERY = "python /var/www/vihu/py/motoboy.py";
    const PORTAO1 = "python /var/www/vihu/py/portao_l.py";
    const PORTAO2 = "python /var/www/vihu/py/pulsarele.py";
    const INVALIDO = "python /var/www/vihu/py/recusado.py";

    public function syncDown(Request $request)
    {
        $uid = Carbon::now()->timestamp . '.' . rand();
        $ip = $_SERVER['REMOTE_ADDR'];
        $acessos = ['moradores' => null, 'visitantes' => null ];

        Log::create(['uid'=>$uid, 'description' => 'Inicio', 'type' => 'syncDown_v1', 'ip' => $ip]);

        //VERIFICA SE O IP EXISTE NA TABELA DE DNS
        $dns = DNS::where('ip', $ip)->first();        
        if (!is_object($dns)) {
            Log::create(['uid'=>$uid, 'description' => 'IP não encontrado', 'type' => 'syncDown_v1', 'ip' => $ip]);
            return response()->json([
                "message" => "Não autorizado."
            ], 401);
        }

        // Log::create(['uid'=>$uid, 'description' => 'IP encontrado para condomínio: ' . $dns->condominiums_id, 'type' => 'syncDown_v1']);

        //PEGA O CONDOMINIO DO IP SELECIONADO
        $condominium = Condominium::find($dns->condominiums_id);        
        if (!is_object($condominium)) {
            Log::create(['uid'=>$uid, 'description' => 'Condomínio não encontrado: ' . $dns->toJSON(), 'type' => 'syncDown_v1', 'ip' => $ip]);
            return response()->json([
                "message" => "Não autorizado."
            ], 401);
        }

        // Log::create(['uid'=>$uid, 'description' => 'Condomínio encontrado: ' . $condominium->name, 'type' => 'syncDown_v1']);

        //PEGA OS ACESSOS DO CONDOMINIO
        $acessos['condominio'] = $condominium->toArray();
        // Log::create(['uid'=>$uid, 'description' => 'Guardou dados do condominio', 'type' => 'syncDown_v1']);

        $acessos['visitantes'] = Access::where('status', 'ativo')->where('condominiums_id', $condominium->id)->get()->toArray();
        // Log::create(['uid'=>$uid, 'description' => 'Guardou acesso dos visitantes', 'type' => 'syncDown_v1']);

        $acessos['moradores'] = User::whereHas('condominiums', function ($query) use ($condominium) {
            $query->where('id', $condominium->id);
        })->get()->makeVisible('password')->toArray();

        Log::create(['uid'=>$uid, 'description' => 'Sync OK - '.$condominium->name.': ' . count($acessos['moradores']) . '|' . count($acessos['visitantes']), 'type' => 'syncDown_v1', 'ip' => $ip]);
        
        return response()->json([
            "data" => $acessos
        ], 200);
    }

    public function syncUp(Request $request)
    {
        $updated = 0;
        $notFound = 0;
        $ip = $_SERVER['REMOTE_ADDR'];
        $codes = json_decode($request->getContent());
        $uid = Carbon::now()->timestamp . '.' . rand();

        //VERIFICA SE O IP EXISTE NA TABELA DE DNS
        $dns = DNS::where('ip', $ip)->first();        
        if (!is_object($dns)) {
            Log::create(['uid'=>$uid, 'description' => 'IP não encontrado', 'type' => 'syncUp_v1', 'ip' => $ip]);
            return response()->json([
                "message" => "Não autorizado."
            ], 401);
        }

        //PEGA O CONDOMINIO DO IP SELECIONADO
        $condominium = Condominium::find($dns->condominiums_id);        
        if (!is_object($condominium)) {
            Log::create(['uid'=>$uid, 'description' => 'Condomínio não encontrado: ' . $dns->toJSON(), 'type' => 'syncUp_v1', 'ip' => $ip]);
            return response()->json([
                "message" => "Não autorizado."
            ], 401);
        }

        if ($codes != '') {
            foreach ($codes->accesses as $item) {
                $acesso = Access::find($item->id);

                if ($acesso) {
                    $acesso->status = $item->status;
                    $acesso->validations = $item->validations;
                    $acesso->gates = json_encode($item->gates);
                    $acesso->save();

                    $updated++;
                } else {
                    $notFound++;
                }
            }
        }

        Log::create(['uid'=>$uid, 'description' => 'Resultado da sincronização: updated:' . $updated . ' | not_found:' . $notFound, 'type' => 'syncUp_v1', 'ip' => $ip]);
        
        return response()->json([
            "updated" => $updated,
            "not_found" => $notFound
        ], 200);      
    }

    public function access(Request $request, VoiceController $voice, SMSController $sms)
    {
        $uid = Carbon::now()->timestamp;
        $ip = $_SERVER['REMOTE_ADDR'];
        $code = $request->route('code');
        $keyboard = $request->route('keyboard');

        Log::create(['uid'=>$uid, 'description' => '1 - ' . $code, 'type' => 'general']);

        //VERIFICA SE O IP EXISTE NA TABELA DE DNS
        $dns = DNS::where('ip', $ip)->first();        
        if (!is_object($dns)) {
            LogAccess::create([
                'access_id' => NULL,
                'condominiums_id' => NULL,
                'code' => $code,
                'ip' => $ip,
                'status' => 4
            ]);

            return $this->resultEncode(self::INVALIDO);
        }

        Log::create(['uid'=>$uid, 'description' => '2 - ' . $code, 'type' => 'general']);
        
        //PEGA O CONDOMINIO DO IP SELECIONADO
        $condominium = Condominium::find($dns->condominiums_id);

        //VERIFICA SE A SENHA É 123456 E RETORNA INVALIDO
        if ($code == '123456') {
            LogAccess::create([
                'access_id' => NULL,
                'condominiums_id' => $condominium->id,
                'code' => $code,
                'ip' => $ip,
                'status' => 2
            ]);

            return $this->resultEncode(self::INVALIDO);
        }

        //VERIFICA SE O CODIGO EXISTE PARA O MORADOR DO CONDOMINIO QUE SOLICITOU A VALIDAÇÃO
        $moradores = User::whereHas('condominiums', function ($query) use ($condominium) {
            $query->where('id', $condominium->id);
        })->get();
        foreach ($moradores as $morador) {
            if (Hash::check($code, $morador->password)) {

                LogResident::create([
                    'user_id' => $morador->id,
                    'condominiums_id' => $condominium->id,
                    'keyboard' => $keyboard,
                    'ip' => $ip
                ]);        

                return base64_encode("python /var/www/vihu/py/portao_l.py");
            }
        }

        Log::create(['uid'=>$uid, 'description' => '3 - ' . $code, 'type' => 'general']);
        
        //VERIFICA SE O CODIGO EXISTE PARA O CONDOMINIO QUE SOLICITOU A VALIDAÇÃO
        $acesso = Access::where('code', '=', $code)->where('status', 'ativo')->where('condominiums_id', $condominium->id)->with('condominium')->first();      
        if (!is_object($acesso)) {
            LogAccess::create([
                'access_id' => NULL,
                'condominiums_id' => $condominium->id,
                'code' => $code,
                'ip' => $ip,
                'status' => 2
            ]);

            return $this->resultEncode(self::INVALIDO);
        }

        //VERIFICA SE O CODIGO JA FOI DIGITADO NESSE TECLADO
        if ($keyboard && $acesso->gates) {
            $_gates = json_decode($acesso->gates, true);
            foreach ($_gates as $gate) {
                if ($gate['gate'] == $keyboard) {
                    LogAccess::create([
                        'access_id' => NULL,
                        'condominiums_id' => $condominium->id,
                        'code' => $code,
                        'ip' => $ip,
                        'status' => 5
                    ]);
        
                    return $this->resultEncode(self::INVALIDO);
                }
            }
        }

        Log::create(['uid'=>$uid, 'description' => '4 - ' . $code, 'type' => 'general']);

        $horario = new HourValidator($acesso);
        if ($horario->isValid()) {

            $gate = [
                'gate' => $keyboard,
                'datetime' => Carbon::now()->toDateTimeString()
            ];
            
            if ((($acesso->type == 'delivery') || ($acesso->validations == 0)) && (!$acesso->exit) ) {
                $voice->sendToDweller($acesso);
                $sms->sendToDweller($acesso);
            }

            if ($acesso->type == 'delivery') {
                $acesso->status = 2;
                $acesso->validations = 1;
                $acesso->gates = json_encode([$gate]);
                $acesso->save();

                LogAccess::create([
                    'access_id' => $acesso->id,
                    'condominiums_id' => $condominium->id,
                    'code' => $code,
                    'ip' => $ip,
                    'status' => 1
                ]);

                return $this->resultEncode(self::DELIVERY);
            } else {
                if (($acesso->condominium->number_of_gates == $acesso->validations + 1) || ($keyboard == $acesso->condominium->number_of_gates) || ($acesso->exit)) {
                    $acesso->status = 2;
                }

                if ($acesso->gates) {
                    $_gates = json_decode($acesso->gates, true);
                    array_push($_gates, $gate);
                    $acesso->gates = json_encode($_gates);
                } else {
                    $acesso->gates = json_encode([$gate]);
                }

                $acesso->validations = $acesso->validations + 1;
                $acesso->save();

                if ($acesso->type == 'visitante') {
                    LogAccess::create([
                        'access_id' => $acesso->id,
                        'condominiums_id' => $condominium->id,
                        'code' => $code,
                        'ip' => $ip,
                        'status' => 1
                    ]);

                    return ($acesso->validations > 1 || $acesso->exit) ? $this->resultEncode(self::PORTAO1) : $this->resultEncode(self::PORTAO2);
                } else if ($acesso->type == 'festa') {
                    LogAccess::create([
                        'access_id' => $acesso->id,
                        'condominiums_id' => $condominium->id,
                        'code' => $code,
                        'ip' => $ip,
                        'status' => 1
                    ]);

                    return ($acesso->validations > 1 || $acesso->exit) ? $this->resultEncode(self::PORTAO1) : $this->resultEncode(self::PORTAO2);
                }
            }
        }

        LogAccess::create([
            'access_id' => $acesso->id,
            'condominiums_id' => $condominium->id,
            'code' => $code,
            'ip' => $ip,
            'status' => 3
        ]);

        return $this->resultEncode(self::INVALIDO);
    }

    public function create(Request $request, SMSController $sms)
    {
        $uid = Carbon::now()->timestamp;
        $user = JWTAuth::toUser(JWTAuth::getToken());
        $unit = null;

        Log::create(['uid'=>$uid, 'description' => 'CREATE ACCESS 1: ' . $user->toJSON(), 'type' => 'create_access']);
        
        if ($user->units()->count() == 0) {
            return response()->json([
                "message" => "Não foi possível criar acesso para o condomínio informado."
            ],401);
        } else {        
            foreach ($user->units()->get() as $_unit) {
                if ($request->condominioId == $_unit->condominiums_id) {
                    $unit = $_unit;
                }
            }
            if (!$unit) {
                return response()->json([
                    "message" => "Não foi possível criar acesso para o condomínio informado."
                ],401);                    
            }
        }
        
        Log::create(['uid'=>$uid, 'description' => 'CREATE ACCESS 2: ' . $user->units()->get()->toJSON(), 'type' => 'create_access']);

        do {
            $code = rand(100000,999999);
        } while (!$this->isUnique($unit->condominiums_id, $code));

        Log::create(['uid'=>$uid, 'description' => 'CREATE ACCESS 3: ' . $code, 'type' => 'create_access']);

        $access = new Access;
        $access->users_id = $user->id;
        $access->visitor_name = ($request->tipo == 2) ? $user->name : $request->name;
        $access->visitor_mobile = ($request->tipo == 2) ? $user->mobile : $request->celular;
        $access->condominiums_id = $request->condominioId;
        $access->date = Carbon::createFromFormat('Y-m-d', $request->dataIni)->toDateString();
        $access->time = $request->horaIni;
        $access->code = $code;
        $access->type = $request->tipo;
        $access->status = 'ATIVO';
        $access->exit = ($request->onlyExiting == 'S') ? 1 : 0;
        $access->save();

        Log::create(['uid'=>$uid, 'description' => 'CREATE ACCESS 4: ' . $access->toJSON(), 'type' => 'create_access']);

        if ($request->tipo == 2) {
            Log::create(['uid'=>$uid, 'description' => 'CREATE ACCESS 5: DELIVERY OK', 'type' => 'create_access']);

            return response()->json([
                "message" => 'Solicitação realizada com sucesso!<br>Sua senha é <strong>' . $code . '</strong>',
                "code" => "$code"
            ],200);
        } else {
            Log::create(['uid'=>$uid, 'description' => 'CREATE ACCESS 5: PREPARE TO SEND SMS', 'type' => 'create_access']);

            $sms->sendToAccess($access);
            
            Log::create(['uid'=>$uid, 'description' => 'CREATE ACCESS 6: ACESSO OK', 'type' => 'create_access']);

            return response()->json([
                "message" => "Acesso enviado com sucesso.",
                "code" => "$code"
            ],200);
        }
    }

    public function party(Request $request, SMSController $sms)
    {
        $user = JWTAuth::toUser(JWTAuth::getToken());
        $unit = null;

        if ($user->units()->count() == 0) {
            return response()->json([
                "message" => "Não foi possível criar acesso para o condomínio informado."
            ],401);
        } else {        
            foreach ($user->units()->get() as $_unit) {
                if ($request->condominioId == $_unit->condominiums_id) {
                    $unit = $_unit;
                }
            }
            if (!$unit) {
                return response()->json([
                    "message" => "Não foi possível criar acesso para o condomínio informado."
                ],401);                    
            }
        }        

        foreach ($request->visitors as $visitor) {
            do {
                $code = rand(100000,999999);
            } while (!$this->isUnique($unit->condominiums_id, $code));

            $access = new Access;
            $access->users_id = $user->id;
            $access->visitor_name = $visitor['nome'];
            $access->visitor_mobile = $visitor['celular'];
            $access->condominiums_id = $request->condominioId;
            $access->date = Carbon::createFromFormat('Y-m-d', $request->dataIni)->toDateString();
            $access->time = $request->horaIni;
            $access->code = $code;
            $access->type = 3;
            $access->status = 'ATIVO';
            $access->save();
            
            $sms->sendToAccess($access);
        }
            
        return response()->json([
            "message" => "Acessos enviados com sucesso."
        ],200);
    }

    private function isUnique($condominium_id, $code)
    {
        //VERIFICA SE EXISTE O CODIGO ATIVO PARA O CONDOMINIO
        $unique = Access::where('condominiums_id', $condominium_id)->where('status', 'ativo')->where('code', $code)->first();
        if ($unique && $unique->status == 'ativo') {
            return false;
        }

        //VERIFICA SE O CODIGO EXISTE PARA ALGUM MORADOR DO CONDOMINIO
        $moradores = User::whereHas('condominiums', function ($query) use ($condominium_id) {
            $query->where('id', $condominium_id);
        })->get();
        foreach ($moradores as $morador) {
            if (Hash::check($code, $morador->password)) {
                return false;
            }
        }

        return true;
    }

    private function resultEncode($content) {
        return base64_encode($content);
    }
}