<?php

namespace App\Http\Controllers;

use App\Condominium;
use App\Units;
use App\Vehicles;
use Illuminate\Http\Request;
use Validator;

class ApiUnitController extends Controller implements CrudInterface
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $units = Units::with(['condominium','block'])->get();

        $data = "Não há unidades para serem listados.";

        if ($units->count() > 0 ) {
            $data = $units->toArray();
        }

        return response()->json([
            "items" => $units->count(),
            "data" => $data
        ],200);
    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "block_id" => "required",
            "condominiums_id" => "required|max:255",
            "number" => "required|max:255",
            "extension_number" => "required|max:255"
        ]);

        if ($validator->fails()) {
            return  response()->json([
                "message" => "Não foi possível criar unidade",
                "errors" => [
                    $validator->errors()
                ]
            ],400);
        }

        $condominium = Condominium::find($request->condominiums_id);

        if (!is_object($condominium)) {
            return  response()->json([
                "message" => "Condomínio não encontrado",
            ],400);
        }

        $block = $condominium->blocks()->where('id', $request->block_id)->first();

        if (!is_object($block)) {
            return  response()->json([
                "message" => "Bloco não encontrado",
            ],400);
        }

        $unit = new Units();
        $unit->number = $request->number;
        $unit->block_id = $request->block_id;
        $unit->condominiums_id = $request->condominiums_id;
        $unit->extension_number = $request->extension_number;
        $unit->rfid_garage_keys = $request->rfid_garage_keys;
        $unit->save();

        return  response()->json([
            "message" => "Unidade criada com sucesso."
        ],200);
    }

    /**
     * @param $id
     */
    public function show($id)
    {
        $unit = Units::with('vehicles')->where('id', $id)->first();

        if (is_object($unit)) {
            return response()->json([
                "unit" => $unit
            ],200);
        }

        return  response()->json([
            "message" => "Unidade não encontrado",
        ],400);
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
        $unit = Units::find($id);
        if (!is_object($unit)) {
            return  response()->json([
                "message" => "Unidade não encontrado",
            ],400);
        }

        $validator = Validator::make($request->all(), [
            "block_id" => "required",
            "condominiums_id" => "required|max:255",
            "number" => "required|max:255",
            "extension_number" => "required|max:255"
        ]);

        if ($validator->fails()) {
            return  response()->json([
                "message" => "Não foi possível editar unidade",
                "errors" => [
                    $validator->errors()
                ]
            ],400);
        }

        $condominium = Condominium::find($request->condominiums_id);
        if (!is_object($condominium)) {
            return  response()->json([
                "message" => "Condomínio não encontrado",
            ],400);
        }

        $block = $condominium->blocks()->where('id', $request->block_id)->first();
        if (!is_object($block)) {
            return  response()->json([
                "message" => "Bloco não encontrado",
            ],400);
        }

        $unit->number = $request->number;
        $unit->block_id = $request->block_id;
        $unit->condominiums_id = $request->condominiums_id;
        $unit->extension_number = $request->extension_number;
        $unit->rfid_garage_keys = $request->rfid_garage_keys;
        $unit->save();

        Vehicles::where('units_id', $id)->delete();
        foreach($request->vehicles as $vehicle) {
            Vehicles::create([
                'units_id' => $id,
                'automaker' => $vehicle['automaker'],
                'model' => $vehicle['model'],
                'color' => $vehicle['color'],
                'license_plate' => $vehicle['license_plate']
            ]);
        }

        return  response()->json([
            "message" => "Unidade alterada com sucesso."
        ],200);
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function patch(Request $request, $id)
    {
        // TODO: Implement patch() method.
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $unit = Units::find($id);

        if (!is_object($unit)) {
            return  response()->json([
                "message" => "Unidade não encontrado",
            ],400);
        }

        $unit->delete();

        return  response()->json([
            "message" => "Unidade excluído com sucesso."
        ],200);
    }
}
