<?php

namespace App\Http\Controllers;

use App\Log;
use App\DNS;
use App\Resale;
use App\Condominium;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Validator;

class ApiCondominiumController extends Controller implements CrudInterface
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $condominiums = Condominium::with('resale')->get();

        $data = "Não há condomínios para serem listados.";

        if ($condominiums->count() > 0 ) {
            $data = $condominiums->toArray();
        }

        return response()->json([
            "items" => $condominiums->count(),
            "data" => $data
        ],200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            "resale_id" => "required",
            "name" => "required|max:255",
            "address" => "required|max:255",
            "number" => "required",
            "district" => "max:255",
            "city" => "max:255",
            "state" => "max:255",
            "cep" => "max:255",
            "number_of_gates" => "required"
        ]);

        if ($validator->fails()) {
            return  response()->json([
                "message" => "Não foi possível criar o usuário",
                "errors" => [
                    $validator->errors()
                ]
            ],400);
        }

        $resale = Resale::find($request->resale_id);
        if (!is_object($resale)) {
            return  response()->json([
                "message" => "ID de revenda não encontrado.",
            ],400);
        }

        $condominium = new Condominium;
        $condominium->resale_id = $request->resale_id;
        $condominium->name = $request->name;
        $condominium->address = $request->address;
        $condominium->number = $request->number;
        $condominium->complement = $request->complement;
        $condominium->district = $request->district;
        $condominium->city = $request->city;
        $condominium->state = $request->state;
        $condominium->cep = $request->cep;
        $condominium->status = 1;
        $condominium->number_of_gates = $request->number_of_gates;
        $condominium->application_features = $request->application_features;
        $condominium->communication_type = $request->communication_type;
        $condominium->lat = $request->lat;
        $condominium->lng = $request->lng;
        $condominium->password = bcrypt(env('VIHUBOX_PWD'));
        $condominium->save();

        foreach ($request->dns as $dnsItem) {
            if ($dnsItem['dns'] != '') {
                $dns = new DNS;
                $dns->condominiums_id = $condominium->id;
                $dns->dns = $dnsItem['dns'];
                $dns->ip = $dnsItem['ip'];
                $dns->save();
            }
        }

        return response()->json([
            "message" => "Condomínio criado com sucesso."
        ],201);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $condominium = Condominium::with('dns')->find($id);

        if (is_object($condominium)) {
            return response()->json([
                "condominium" => $condominium
            ],200);
        }

        return  response()->json([
            "message" => "Condomínio não encontrado",
        ],400);
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
        $condominium = Condominium::find($id);

        if (!is_object($condominium)) {
            return  response()->json([
                "message" => "Condomínio não encontrado",
            ],400);
        }

        $validator = Validator::make($request->all(), [
            'resale_id' => 'required',
            'name' => 'required',
            'address' => 'required|max:100',
            'district' => 'max:50',
            'city' => 'required|max:100',
            'state' => 'required|max:100',
            'cep' => 'required|max:100',
            "number_of_gates" => "required"
        ]);

        if ($validator->fails()) {
            return  response()->json([
                "message" => "Não foi possível editar a revenda.",
                "errors" => [
                    $validator->errors()
                ]
            ],400);
        }

        $condominium->resale_id = $request->resale_id;
        $condominium->name = $request->name;
        $condominium->address = $request->address;
        $condominium->number = $request->number;
        $condominium->district = $request->district;
        $condominium->city = $request->city;
        $condominium->state = $request->state;
        $condominium->cep = $request->cep;
        $condominium->number_of_gates = $request->number_of_gates;
        $condominium->application_features = $request->application_features;
        $condominium->communication_type = $request->communication_type;
        $condominium->lat = $request->lat;
        $condominium->lng = $request->lng;
        $condominium->password = bcrypt(env('VIHUBOX_PWD'));
        $condominium->status = "ATIVO";
        $condominium->save();

        DNS::where('condominiums_id', $condominium->id)->delete();
        foreach ($request->dns as $dnsItem) {
            if ($dnsItem['dns'] != '') {
                $dns = new DNS;
                $dns->condominiums_id = $condominium->id;
                $dns->dns = $dnsItem['dns'];
                $dns->ip = $dnsItem['ip'];
                $dns->save();
            }
        }        

        return  response()->json([
            "message" => "Condomínio alterada com sucesso."
        ],200);
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function patch(Request $request, $id)
    {
        // TODO: Implement patch() method.
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $condominium = Condominium::find($id);

        if (!is_object($condominium)) {
            return  response()->json([
                "message" => "Condomínio não encontrado",
            ],400);
        }

        $condominium->status = 2;
        $condominium->save();
        $condominium->delete();

        DNS::where('condominiums_id', $id)->delete();

        return  response()->json([
            "message" => "Condomínio excluído com sucesso."
        ],200);
    }

    public function ddnsUpdate($dns = '') {
        $count = 0;
        $uid = Carbon::now()->timestamp;
        
        if ($dns != '') {
            $ip = exec("dig +short " . $dns);
            // $ip = gethostbyname($dns);

            return response()->json([
                "ip" => $ip
            ],200);            
        } else {
            $condominiums = Condominium::where('status', 'ATIVO')->with('dns')->inRandomOrder()->get();
            
            foreach ($condominiums as $condominium) {
                foreach ($condominium->dns as $dns) {
                    $ip = exec("dig +short " . $dns->dns);
                    Log::create(['uid'=>$uid, 'description' => $condominium->name . ' - ' . $dns->dns . ' <> ' . $ip, 'type' => 'DNS']);
    
                    if ($dns != '' && filter_var($ip, FILTER_VALIDATE_IP) ) {
                        $dns->ip = $ip;
                        $dns->save();
                        $count++;
                    }
                }
            }

            return response()->json([
                "updated" => $count
            ],200);
        }
    }
}