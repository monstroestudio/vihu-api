<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param $data
     * @param String $action
     * @param String $model
     */
    protected function formatedResponse($data, String $action, String $entidade)
    {
        switch ($action) {
            case "add":
                return $this->formatAdd($data, $entidade);
                break;
            case "list":
                return $this->formatList($data, $entidade);
            case "delete":
                return $this->formatDelete($data, $entidade);
            default:
                echo 'é preciso definir ';
        }
    }

    private function formatAdd($data, $entidade)
    {
        if($data && !is_object($data)) {
            return response()->json([
                "Message" => "$entidade criada com sucesso."
            ],201);
        }

        return  response()->json([
            "Message" => "Não foi possível criar a ".strtolower($entidade).".",
            "Errors" => [
                $data->messages()
            ]
        ],400);
    }
}
