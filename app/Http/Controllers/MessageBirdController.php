<?php

namespace App\Http\Controllers;

use MessageBird\Client;
use MessageBird\Objects\Message;
use MessageBird\Objects\VoiceMessage;
use Illuminate\Http\Request;

class MessageBirdController extends Controller
{
    private $messageBird;

    public function __construct() {
        $this->messageBird = new Client(env('MESSAGEBIRD_KEY'));
    }

    public function sendURA($voice) {
        $voiceMessage = new VoiceMessage();
        $voiceMessage->originator = 'MessageBird';
        $voiceMessage->recipients = ['55'.$voice->mobile];
        $voiceMessage->body = $voice->message;
        $voiceMessage->language = 'pt-br';
        $voiceMessage->voice = 'female';

        try {
            return $this->messageBird->voicemessages->create($voiceMessage);
        } catch (\Exception $exception) {
            return '';
        }
    }

    public function sendSMS($sms) {
        $message = new Message();
        $message->originator = 'MessageBird';
        $message->recipients = ['+55'.$sms->mobile];
        $message->body = $sms->message;
        $message->datacoding = 'auto';

        return $this->messageBird->messages->create($message);
    }

    public function sendSMS2($_message, $phone) {
        $phone = [str_replace(' ', '', str_replace(')', '', str_replace('(', '', str_replace('-', '', $phone))))];

        $message = new Message();
        $message->originator = 'MessageBird';
        $message->recipients = $phone;
        $message->body = $_message;
        $message->datacoding = 'auto';

        return $this->messageBird->messages->create($message);
    }    
}
