<?php

namespace App\Http\Controllers;

use Hash;
use Carbon\Carbon;
use App\Log;
use App\SMS;
use App\User;
use Illuminate\Http\Request;
// use App\Http\Controllers\NexmoController;
use App\Http\Controllers\MessageBirdController;
use App\Http\Controllers\SinchController;

class SMSController extends Controller
{
    private $nexmo;
    private $zenvia;
    private $messageBird;
    
    public function __construct(MessageBirdController $messageBird, ZenviaController $zenvia) {
        $this->zenvia = $zenvia;
        $this->messageBird = $messageBird;
    }
    
    private function save($user_id, $access_id, $phone, $message){
        $sms = new SMS;
        $sms->users_id = $user_id;
        $sms->access_id = $access_id;
        $sms->mobile = $phone;
        $sms->message = $message;
        $sms->status = 1;
        $sms->save();

        return $sms;
    }

    public function sendToValidation($user, $pin, $msg){
        $uid = Carbon::now()->timestamp;
      
        Log::create(['uid'=>$uid, 'description' => '1: ' . $user->toJSON(), 'type'=>'sms']);

        $message = $pin.' '.$msg;
        $sms = $this->save($user->id, null, $user->mobile, $message);
        
        Log::create(['uid'=>$uid, 'description' => '2: ' . $sms->toJSON(), 'type'=>'sms']);

        $this->send();
        return true;
    }

    public function sendToAccess($data){
        
        $uid = Carbon::now()->timestamp;
        $user = User::find($data->users_id);

        Log::create(['uid'=>$uid, 'description' => '1: ' . $user->toJSON(), 'type'=>'sms']);

        $message = $data->code.' código VIHU de acesso ao portão de visitantes enviado por '.$user->name.' para ' . Carbon::createFromFormat('Y-m-d', $data->date)->format('d/m/Y') . ' a partir das ' . $data->time;
        // $message = 'Olá ' . $data->visitor_name . ' o morador '.$user->name.' enviou a(s) seguinte(s) senha(s) para acesso ao condomínio '.$data->condominium->name.' para o dia ' . Carbon::createFromFormat('Y-m-d', $data->date)->format('d/m/Y') . ' as ' . $data->time . ': ' . $data->code;
        $sms = $this->save($data->users_id, $data->id, $data->visitor_mobile, $message);
        
        Log::create(['uid'=>$uid, 'description' => '2: ' . $sms->toJSON(), 'type'=>'sms']);
       
        return true;
    }

    public function sendToDweller($data){
        $user = User::find($data->users_id);

        $message = ($data->type == 'delivery') ? 'Olá '.$user->name.', seu delivery acabou de chegar.' : 'Olá '.$user->name.', sua visita '.$data->visitor_name.' acabou de chegar.';
        $sms = $this->save($data->users_id, $data->id, $user->mobile, $message);

        $this->send();
        return true;
    }

    public function sendToConcierge($user, $concierge)
    {
        $message = 'Sr(a) Zelador(a), o morador '.$user->name.' da unidade ' .$user->units->first()->number. ' solicitou o seu contato.';
        $sms = $this->save($user->id, null, $concierge->mobile, $message);

        return $sms;
    }

    public function sendToSafe($user, $data){
        $message = 'Olá '.$data['destinatario'].', o morador '.$user->name.' enviou a senha '.$data['senha'].' do cofre número '.$data['numero'];
        $sms = $this->save($user->id, null, $data['celular'], $message);

        $this->send();
        return true;
    }    

    public function sendToPanic($user, $data){
        if (strtolower(str_slug($data['reason'])) == 'chamar-supervisor') {
            $message = 'Senhor Supervisor. Comparecer no condomínio '.$user->condominiums->first()->name.', no apartamento '.$user->units->first()->number;
        } else if (strtolower(str_slug($data['reason'])) == 'assalto-em-andamento') {
            $message = 'A residência de '.$user->name.', no condomínio '.$user->condominiums->first()->name.', apartamento '.$user->units->first()->number.' está sofrendo um assalto e o botão de pânico foi ativado';
        } else if (strtolower(str_slug($data['reason'])) == 'problema-de-saude') {
            $message = $user->name.' está com problemas de saúde, e acionou o botão de pânico pedindo ajuda, no condomínio '.$user->condominiums->first()->name.', apartamento '.$user->units->first()->number;
        } else {
            return false;
        }
       
        $sms = $this->save($user->id, null, $data['phones'][0], $message);

        $this->send();
        return true;
    }

    public function send() {
        $sms = SMS::where('status', 'pendente')->get();

        foreach ($sms as $item) {
            $success = false;
            $send = $this->zenvia->sendSMS($item);
            
            if ($send === false) {
                $send = $this->messageBird->sendSMS($item);

                if ($send->recipients->totalSentCount == 1) {
                    $success = true;
                }
            } else {
                $success = true;
            }

            if ($success === true) {
                $sms = SMS::find($item->id);
                $sms->status = 2;
                $sms->save();
            }
        }
    } 
}
