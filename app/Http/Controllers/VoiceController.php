<?php

namespace App\Http\Controllers;

use App\Voice;
use Illuminate\Http\Request;
use App\Http\Controllers\NexmoController;
use App\Http\Controllers\MessageBirdController;

class VoiceController extends Controller
{
    private $nexmo;
    private $messageBird;
    
    public function __construct(NexmoController $nexmo, MessageBirdController $messageBird) {
        $this->nexmo = $nexmo;
        $this->messageBird = $messageBird;
    }

    private function save($user_id, $access_id, $phone, $message){
        $voice = new Voice;
        $voice->users_id = $user_id;
        $voice->access_id = $access_id;
        $voice->mobile = str_replace('-', '', str_replace(')', '', str_replace('(', '', str_replace(' ', '', $phone))));
        $voice->message = $message;
        $voice->status = 1;
        $voice->save();

        return $voice;
    }

    public function sendToPanic($user, $data){
        if (strtolower(str_slug($data['reason'])) == 'chamar-supervisor') {
            $message = 'Senhor Supervisor. Comparecer no condomínio '.$user->condominiums->first()->name.', no apartamento '.$user->units->first()->number;
        } else if (strtolower(str_slug($data['reason'])) == 'assalto-em-andamento') {
            $message = 'A residência de '.$user->name.', no condomínio '.$user->condominiums->first()->name.', apartamento '.$user->units->first()->number.' está sofrendo um assalto e o botão de pânico foi ativado';
        } else if (strtolower(str_slug($data['reason'])) == 'problema-de-saude') {
            $message = $user->name.' está com problemas de saúde, e acionou o botão de pânico pedindo ajuda, no condomínio '.$user->condominiums->first()->name.', apartamento '.$user->units->first()->number;
        } else {
            return false;
        }
       
        $voice = $this->save($user->id, null, $data['phones'][0], $message);
        $send = $this->nexmo->sendURA($voice);

        return $send;
    }

    public function sendToDweller($access) {
        if ($access->type == 'delivery') {
            $message = 'Olá '.$access->user->name.', seu delivery acabou de chegar.';
        } else {
            $message = 'Olá '.$access->user->name.', sua visita '.$access->visitor_name.' acabou de chegar.';
        }
        
        $voice = $this->save($access->users_id, $access->id, $access->user->mobile, $message);
        $send = $this->nexmo->sendURA($voice);

        return $send;     
    }
}
