<?php

namespace App\Http\Controllers;

use App\Block;
use App\Condominium;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class ApiBlockController extends Controller implements CrudInterface
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $blocks = Block::with('condominium')->get();

        return response()->json([
            "items" => $blocks->count(),
            "blocks" => $blocks->toArray()
        ],200);
    }

    public function listByCondominium($condominiumsID)
    {
        $blocks = Block::where('condominiums_id', $condominiumsID)->with('condominium')->get();

        return response()->json([
            "items" => $blocks->count(),
            "blocks" => $blocks->toArray()
        ],200);
    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "condominiums_id" => "required",
            "name" => "required|max:255",
            "floors" => "required|max:255",
        ]);

        if ($validator->fails()) {
            return  response()->json([
                "message" => "Não foi possível criar o usuário",
                "errors" => [
                    $validator->errors()
                ]
            ],400);
        }

        $condominium = Condominium::find($request->condominiums_id);

        if (!is_object($condominium)) {
            return response()->json([
                "message" => "Condomínio não encontrado."
            ],400);
        }

        $blockInCondominium = DB::table('block')
                                ->where('condominiums_id', '=', $request->condominiums_id)
                                ->get();

        if ($blockInCondominium->contains('name', $request->name)) {
            return response()->json([
                "message" => "O bloco já existe."
            ],400);
        }

        $block = new Block;
        $block->condominiums_id = $request->condominiums_id;
        $block->name = $request->name;
        $block->floors = $request->floors;
        $block->save();

        return response()->json([
            "message" => "Bloco criado com sucesso."
        ],201);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $block = Block::find($id);

        if (is_object($block)) {
            return response()->json([
                "block" => $block
            ],200);
        }

        return  response()->json([
            "message" => "Bloco não encontrado.",
        ],400);
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
        $block = Block::find($id);

        if (!is_object($block)) {
            return  response()->json([
                "message" => "Bloco não encontrado",
            ],400);
        }

        $validator = Validator::make($request->all(), [
            'condominiums_id' => 'required',
            'name' => 'required|max:100',
            'floors' => 'required|max:100',
        ]);

        if ($validator->fails()) {
            return  response()->json([
                "message" => "Não foi possível editar o bloco.",
                "errors" => [
                    $validator->errors()
                ]
            ],400);
        }

        $block->condominiums_id = $request->condominiums_id;
        $block->name = $request->name;
        $block->floors = $request->floors;
        $block->save();
        
        return  response()->json([
            "message" => "Bloco alterada com sucesso."
        ],200);
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function patch(Request $request, $id)
    {
        // TODO: Implement patch() method.
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $block = Block::find($id);

        if (!is_object($block)) {
            return response()->json([
                "message" => "Bloco não encontrado."
            ],400);
        }

        $block->save();
        $block->delete();

        return  response()->json([
            "message" => "Bloco excluído com sucesso."
        ],200);
    }
}
