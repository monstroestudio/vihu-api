<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SinchController extends Controller
{
    private $key;
    private $secret;
    private $phone_number;

    public function __construct() {
        $this->key = env("SINCH_KEY");
        $this->secret = env("SINCH_SECRET"); 
        $this->phone_number = "551141302957";
    }

    public function sendSMS($sms) {
        $user = "application\\" . $this->key . ":" . $this->secret;    
        $message = json_encode(["message"=>$sms->message]);
        $ch = curl_init('https://messagingapi.sinch.com/v1/sms/55' . $sms->phone);
        curl_setopt($ch, CURLOPT_POST, true);    
        curl_setopt($ch, CURLOPT_USERPWD, $user);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $message);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));    
        $result = curl_exec($ch);

        if(curl_errno($ch)) {    
            echo 'Curl error: ' . curl_error($ch);    
        } else {    
            echo $result;    
        }   
        curl_close($ch);
        
        
        // return Nexmo::message()->send([
        //     'type' => 'unicode',
        //     'to' => '55'.$sms->phone,
        //     'from' => '14422223940',
        //     'text' => $sms->message
        // ]);
    }
}
