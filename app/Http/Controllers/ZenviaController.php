<?php
namespace App\Http\Controllers;

use App\Log;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ZenviaController extends Controller {

    private $headers;

    public function __construct() {
        $this->headers = [
            'Authorization' => 'Basic ' . env('ZENVIA_KEY'),
            'Accept'        => 'application/json',
            'Content-Type'  => 'application/json'
        ];        
    }

    public function sendSMS($sms) {
        $data = [
            'sendSmsRequest' => [
                'from' => '',
                'to' => '55'.$sms->mobile,
                'msg' => $sms->message,
                'id' => $sms->id,
                'callbackOption' => 'NONE'
            ]
        ];

        $client = new Client();
        $response = $client->request('POST', env('ZENVIA_HOST').'/send-sms', [
            'headers' => $this->headers,
            'body' => json_encode($data)
        ]);
        
        return $this->checkResponse($response);
    }

    public function sendSmsMulti($sms) {
        $data = [
            'sendSmsRequest' => [
                'from' => '',
                'to' => '55'.$sms->mobile,
                'msg' => $sms->message,
                'id' => $sms->id,
                'callbackOption' => 1
            ]
        ];

        $client = new Client();
        $response = $client->request('POST', env('ZENVIA_HOST').'/send-sms', [
            'headers' => $this->headers,
            'body' => json_encode($data)
        ]);

        return $this->checkResponse($response);
    }

    private function checkResponse($response) {
        if ($response->getStatusCode() === 200) {
            $smsResponse = json_decode($response->getBody());
            return ($smsResponse->sendSmsResponse->statusCode < 3);
        } else {
            return false;
        }
    }

    public function answer(Request $request) {
    }
}