<?php

namespace App\Http\Controllers;

use Mail;
use JWTAuth;
use Carbon\Carbon;
use App\Access;
use App\LogAccess;
use App\LogResident;
use App\Mail\Report;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    public function accesses() {
        $accesses = LogAccess::with('condominiums')->with('access')->orderBy('id', 'desc')->limit(1000)->get()->toArray();

        return response()->json([
            "data" => $accesses
        ], 200);
    }

    public function residents() {
        $accesses = LogResident::with('condominiums')->with('user')->orderBy('id', 'desc')->limit(1000)->get()->toArray();

        return response()->json([
            "data" => $accesses
        ], 200);
    }

    public function accessByUser(Request $request) {
        $user = JWTAuth::toUser(JWTAuth::getToken());
        
        $accesses = Access::where('users_id', $user->id)->whereBetween('created_at', array($request->dataIni, $request->dataFim))->with('condominium')->orderBy('id', 'desc')->get()->toArray();

        if (count($accesses) > 0) {
            $data = [
                'request' => $request->all(),
                'accesses' => $accesses
            ];

            Mail::to($user)->send(new Report($user, $data));
            
            return response()->json([
                "message" => 'Relatório enviado com sucesso!'
            ], 200);
        } else {
            return response()->json([
                "message" => 'Não foi encontrado nenhum registro para o período selecionado.'
            ], 500);
        }
        
    }
}