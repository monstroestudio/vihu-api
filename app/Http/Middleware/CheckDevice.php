<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;

class CheckDevice
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = JWTAuth::toUser(JWTAuth::getToken());
        
        if ($user->is_admin != 'S') {
            $device = $user->devices()->where('status', 'active')->first();
            if (!$device || !is_array($request->device) || $device->data['uuid'] != $request->device['uuid']) {
                return response()->json(['error'=>'invalid_device'], 401);
            }
        }

        return $next($request);
    }
}
