<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicles extends Model
{
    protected $fillable = [
        'units_id',
        'automaker',
        'model',
        'color',
        'license_plate'
    ];    
}
