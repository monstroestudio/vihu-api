<?php

namespace App\Utils;

/**
* 
*/
class HourValidator
{
	private $before;
	private $after;

	function __construct($access)
	{
		$before = new \DateTime($access->date.' '. $access->time);
		$before->modify('-30 minutes');
		$this->before = $before;

		$after = new \DateTime($access->date.' '. $access->time);
		$after->modify('+3 hours');
		$this->after = $after;
	}

	public function isValid()
	{
		$now = new \DateTime();

		if ($now >= $this->before && $now <= $this->after) {
			return true;
		}

		return false;
	}
}