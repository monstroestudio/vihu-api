<?php

namespace App\Utils;

use App\User;
use App\Access;

/**
* 
*/
class GenerateCode
{
	function __construct() {
    }

    public static function visit($condominium_id) {
        do {
            $code = rand(100000,999999);
        } while (!self::isUnique('visit', $condominium_id, $code));
        
        return $code;
    }

    public static function resident($condominiums) {
        do {
            $code = rand(100000,999999);
        } while (!self::isUnique('resident', $condominiums, $code));

        return $code;
    }

	private static function isUnique($type, $condominium, $code)
	{
        
        if ($type == 'visit') {

            //VERIFICA SE EXISTE O CODIGO ATIVO PARA O CONDOMINIO
            $unique = Access::where('condominiums_id', $condominium)->where('status', 'ativo')->where('code', $code)->first();
            if ($unique && $unique->status == 'ativo') {
                return false;
            }

        } else {

            //VERIFICA SE O CODIGO EXISTE PARA ALGUM MORADOR DO CONDOMINIO
            $morador = User::where('access_code', sha1($code))->whereHas('condominiums', function ($query) use ($condominium) {
                $query->whereIn('id', $condominium);
            })->first();
            if (is_object($morador)) {
                return false;
            }
        }

        return true;
	}
}