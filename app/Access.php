<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Access extends Model
{
    protected $table = 'access';

    public function user(){
        return $this->hasOne('App\User', 'id', 'users_id');
    }

    public function condominium() {
        return $this->hasOne('App\Condominium','id','condominiums_id');
    }
}
