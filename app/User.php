<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'mobile',
        'device',
        'panic',
        'email',
        'password',
        'remember_pin',
        'remember_token',
        'policy',
        'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_pin', 'remember_token', 'access_code',
    ];

    protected $casts = [
        'rfid_keys' => 'array',
        'communication_type' => 'array'
    ];    

    public function resales()
    {
        return $this->belongsToMany('App\Resale', 'resale_users', 'users_id', 'resale_id');        
    }

    public function condominiums()
    {
        return $this->belongsToMany('App\Condominium', 'condominiums_users', 'users_id', 'condominiums_id');        
    }

    public function types()
    {
        return $this->hasMany('App\CondominiumsUsers', 'users_id');
    }
    
    public function units()
    {
        return $this->belongsToMany('App\Units', 'units_users', 'users_id', 'units_id');        
    }
    
    public function devices()
    {
        return $this->hasMany('App\UserDevices', 'id', 'device_id');
    }    
}
