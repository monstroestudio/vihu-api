<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitsUser extends Model
{
    protected $table = 'units_users';

    protected $fillable = [
        'units_id',
        'users_id',
        'type',
    ];
}
