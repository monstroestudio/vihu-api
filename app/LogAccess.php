<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogAccess extends Model
{
    protected $table = "log_access";
    
    protected $fillable = [
        'access_id',
        'condominiums_id',
        'code',
        'keyboard',
        'ip',
        'status'
    ];

    public function access() {
        return $this->hasOne('App\Access', 'id', 'access_id');
    }    

    public function condominiums() {
        return $this->hasOne('App\Condominium', 'id', 'condominiums_id');
    }        
}
