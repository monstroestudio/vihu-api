<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogResident extends Model
{
    protected $table = "log_residents";
    
    protected $fillable = [
        'user_id',
        'condominiums_id',
        'keyboard',
        'type',
        'ip'
    ];

    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }        

    public function condominiums() {
        return $this->hasOne('App\Condominium', 'id', 'condominiums_id');
    }        
}
