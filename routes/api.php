<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: GET,OPTIONS,POST,PUT,DELETE");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, x-xsrf-token");

use App\Log;
use Illuminate\Http\Request;

//AUTENTICAÇÃO DO APP
Route::post('/login', 'ApiAuthController@authenticate');
Route::post('/validate', 'ApiAuthController@validatePIN');

//AUTENTICAÇÃO DA ADMINISTRAÇÃO
Route::post('/login2', 'ApiAuthController@authenticate2');

//RESET DE SENHA
Route::post('/reset-password', 'PasswordController@reset');
Route::post('/reset-password-validade-pin', 'PasswordController@pin');
Route::post('/reset-password-save', 'PasswordController@save');

//ROTAS PARA TECLADOS
Route::get('/access/v2/sync-down', 'AccessController@syncDown');
Route::post('/access/v2/sync-up', 'AccessController@syncUp');
Route::get('/access/v2/{code}/{keyboard?}', 'AccessController@access');
Route::get('/access/key/v2/{code}/{keyboard?}', 'AccessController@accessByKey');

//ROTAS PARA TECLADOS ANTIGOS
Route::get('/access/sync-down', 'AccessController_v1@syncDown');
Route::post('/access/sync-up', 'AccessController_v1@syncUp');
Route::get('/access/{code}/{keyboard?}', 'AccessController_v1@access');
Route::get('/access-by-key/{code}/{keyboard?}', 'AccessController_v1@accessByKey');

//RETORNOS DAS URAS
Route::get('/ura/{id}', 'NexmoController@answer');
Route::get('/ura/callback', function(){
    return true;
});
Route::post('/ura/log', function(Request $request){
    $log = new Log;
    $log->description = json_encode($request->all());
    $log->save();
    
    return response()->json(["message" => "Ok"], 200);
});

//ROTINAS EXECUTADAS PELO SERVIDOR
Route::get('/sms/send', 'SMSController@send');
Route::get('/ddns/update', 'ApiCondominiumController@ddnsUpdate');
Route::get('/internal/maintenance', 'ApiMaintenanceController@maintenance');

//INSTALAÇÃO DO VIHUBOX
Route::post('/vihubox/install', 'ApiAuthController@vihuBoxInstall');

//ROTAS PARA APP E ADMINISTRAÇÃO
Route::group(['middleware' => ['jwt.auth', 'check.device']], function () {
    Route::post('/policy', 'ApiUserController@policy');
    Route::post('/safe', 'ApiUserController@safe');
    Route::post('/concierge', 'ApiUserController@concierge');
    Route::post('/access/create', 'AccessController@create');
    Route::post('/access/party', 'AccessController@party');

    Route::group(['prefix' => '/resales'], function () {
        Route::get('/', 'ApiResaleController@index');
        Route::post('/', 'ApiResaleController@create');
        Route::get('/{resale}', 'ApiResaleController@show');
        Route::put('/{resale}', 'ApiResaleController@update');
        Route::patch('/{resale}', 'ApiResaleController@patch');
        Route::delete('/{resale}', 'ApiResaleController@delete');
    });

    Route::group(['prefix' => '/users'], function () {
        Route::get('/', 'ApiUserController@index');
        Route::get('/{user}', 'ApiUserController@show');
        Route::post('/', 'ApiUserController@create');
        Route::put('/{user}', 'ApiUserController@update');
        Route::delete('/{user}', 'ApiUserController@delete');
        Route::post('/changepwd', 'ApiUserController@changePassword');
        Route::post('/generate-access-code', 'ApiUserController@generateAccessCode');
        Route::post('/validate-password', 'ApiUserController@validatePassword');
        Route::post('/panic', 'ApiUserController@panic');
        Route::post('/panic/save', 'ApiUserController@panicSave');
    });

    Route::group(['prefix' => '/residents'], function () {
        Route::get('/', 'ApiResidentController@index');
        Route::get('/{user}', 'ApiResidentController@show');
        Route::post('/', 'ApiResidentController@create');
        Route::put('/{user}', 'ApiResidentController@update');
        Route::delete('/{user}', 'ApiResidentController@delete');
        Route::post('/changepwd', 'ApiResidentController@changePassword');
        Route::post('/panic', 'ApiResidentController@panic');
        Route::post('/panic/save', 'ApiResidentController@panicSave');
    });

    Route::group(['prefix' => '/condominiums'], function () {
        Route::get('/', 'ApiCondominiumController@index');
        Route::get('/{condominiums}', 'ApiCondominiumController@show');
        Route::post('/', 'ApiCondominiumController@create');
        Route::put('/{condominiums}', 'ApiCondominiumController@update');
        Route::delete('/{user}', 'ApiCondominiumController@delete');
        Route::get('/dns/{dns?}', 'ApiCondominiumController@ddnsUpdate');
    });

    Route::group(['prefix' => '/blocks'], function () {
        Route::get('/', 'ApiBlockController@index');
        Route::get('/{block}', 'ApiBlockController@show');
        Route::get('/condominium/{condominium}', 'ApiBlockController@listByCondominium');
        Route::post('/', 'ApiBlockController@create');
        Route::put('/{block}', 'ApiBlockController@update');
        Route::delete('/{block}', 'ApiBlockController@delete');
    });

    Route::group(['prefix' => '/units'], function () {
        Route::get('/', 'ApiUnitController@index');
        Route::get('/{unit}', 'ApiUnitController@show');
        Route::post('/', 'ApiUnitController@create');
        Route::put('/{unit}', 'ApiUnitController@update');
        Route::delete('/{unit}', 'ApiUnitController@delete');
    });

    Route::group(['prefix' => '/reports'], function () {
        Route::post('/access-user', 'ReportsController@accessByUser');
        Route::get('/accesses', 'ReportsController@accesses');
        Route::get('/residents', 'ReportsController@residents');
    });    
});

//ROTAS PARA LIKAR TODOS MORADORES A UM CONDOMINIO (DEV)
//Route::get('/dev/user-to-units/{unit}', 'ApiUserController@usersToUnit');