<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrador',
            'email' => 'admin@vihu.com.br',
            'password' => bcrypt('vihu@dmin123'),
            'mobile' => '11999999999',
            'is_admin' => 'S'
        ]);

        //factory('App\User', 300)->create();
    }
}
