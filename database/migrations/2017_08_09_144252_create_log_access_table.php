<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_access', function(Blueprint $table){
            $table->increments('id');
            $table->integer('access_id')->nullable();
            $table->integer('condominiums_id')->nullable();
            $table->string('code');
            $table->integer('keyboard')->nullable();
            $table->string('ip');
            $table->enum('status', ['valid', 'invalid', 'expired', 'ip_invalid', 'gate_repeated']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_access');
    }
}
