<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('users', 'device_uuid')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('device_uuid');
            });
        }

        if (!Schema::hasColumn('users', 'device_id')) {
            Schema::table('users', function (Blueprint $table) {
                $table->integer('device_id')->nullable()->references('id')->on('user_devices')->after('is_admin');
            });
        }        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('users', 'device_uuid')) {
            Schema::table('users', function (Blueprint $table) {
                $table->uuid('device_uuid')->nullable();
            });
        }

        if (Schema::hasColumn('users', 'device_id')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('device_id');
            });
        }
    }
}
