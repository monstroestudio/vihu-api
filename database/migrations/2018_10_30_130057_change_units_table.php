<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('units', 'extension_number')) {
            Schema::table('units', function (Blueprint $table) {
                $table->string('extension_number')->nullable()->after('number');
                $table->json('rfid_garage_keys')->nullable()->after('extension_number');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('units', 'extension_number')) {
            Schema::table('units', function (Blueprint $table) {
                $table->dropColumn('extension_number');
                $table->dropColumn('rfid_garage_keys');
            });
        }
    }
}
