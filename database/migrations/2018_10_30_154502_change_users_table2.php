<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUsersTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('users', 'keys')) {
            Schema::table('users', function (Blueprint $table) {
                $table->json('rfid_keys')->nullable()->after('device_id');
                $table->json('communication_type')->nullable()->after('rfid_keys');
            });
        }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'keys')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('rfid_keys');
                $table->dropColumn('communication_type');
            });
        }    
    }
}
