<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Change2CondominiumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('condominiums', 'password')) {
            Schema::table('condominiums', function (Blueprint $table) {
                $table->string('password')->after('lng');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('condominiums', 'password')) {
            Schema::table('condominiums', function (Blueprint $table) {
                $table->dropColumn('password');
            });
        }
    }
}