<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users');
            $table->integer('condominiums_id')->unsigned();
            $table->foreign('condominiums_id')->references('id')->on('condominiums');            
            $table->string('visitor_name');
            $table->string('visitor_mobile');
            $table->date('date');
            $table->time('time');
            $table->enum('status', ['ativo','used','expired']);
            $table->integer('validations')->default(0);
            $table->string('code');
            $table->enum('type', ['visitante', 'delivery', 'festa']);
            $table->tinyInteger('exit')->default(0);
            $table->json('gates')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access');
    }
}
