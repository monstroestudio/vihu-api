<?php

use App\DNS;
use App\Condominium;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangesCondominiumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $condominiums = Condominium::all();
        foreach ($condominiums as $condominium) {
            if ($condominium->dns != '') {
                DNS::create([
                    'condominiums_id' => $condominium->id,
                    'dns' => $condominium->dns,
                    'ip' => $condominium->ip
                ]);
            }
        }
        
        if (Schema::hasColumn('condominiums', 'dns')) {
            Schema::table('condominiums', function (Blueprint $table) {
                $table->dropColumn('dns');
                $table->dropColumn('ip');
            });
        }

        if (Schema::hasColumn('condominiums', 'concierge_type')) {
            Schema::table('condominiums', function (Blueprint $table) {
                $table->dropColumn('concierge_type');
                $table->integer('number_of_gates')->after('cep');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('condominiums', 'dns')) {
            Schema::table('condominiums', function (Blueprint $table) {
                $table->string('dns');
                $table->string('ip');
            });
        }

        if (!Schema::hasColumn('condominiums', 'concierge_type')) {
            Schema::table('condominiums', function (Blueprint $table) {
                $table->enum('concierge_type', ['simples','clausura']);
                $table->dropColumn('number_of_gates');
            });
        }
    }
}
