<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms', function (Blueprint $table) {
              $table->increments('id');
              $table->integer('users_id')->unsigned();
              $table->foreign('users_id')->references('id')->on('users');
              $table->integer('access_id')->unsigned()->nullable();
              $table->foreign('access_id')->references('id')->on('access');
              $table->string('mobile');
              $table->string('message');
              $table->string('status');
              $table->timestamps();
              $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms');
    }
}
