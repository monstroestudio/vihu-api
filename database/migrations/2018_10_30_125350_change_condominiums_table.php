<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCondominiumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('condominiums', 'communication_type')) {
            Schema::table('condominiums', function (Blueprint $table) {
                $table->json('application_features')->after('status');
                $table->json('communication_type')->after('application_features');
                $table->float('lat',10,6)->after('communication_type');
                $table->float('lng',10,6)->after('lat');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('condominiums', 'communication_type')) {
            Schema::table('condominiums', function (Blueprint $table) {
                $table->dropColumn('application_features');
                $table->dropColumn('communication_type');
                $table->dropColumn('lat');
                $table->dropColumn('lng');
            });
        }
    }
}
