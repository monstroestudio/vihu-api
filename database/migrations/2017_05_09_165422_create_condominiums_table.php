<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCondominiumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condominiums', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('resale_id')->unsigned();
            $table->foreign('resale_id')->references('id')->on('resale');
            $table->string('name');
            $table->string('address');
            $table->char('number', 10);
            $table->string('complement')->nullable();            
            $table->string('district');
            $table->string('city');
            $table->string('state');
            $table->string('cep');
            $table->string('status');
            $table->string('dns')->nullable();
            $table->string('ip')->nullable();
            $table->enum('concierge_type', ['simples','clausura']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condominiums');
    }
}
